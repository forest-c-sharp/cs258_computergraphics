#ifndef READMODULE_H
#define READMODULE_H

#include "module.h"

//forward declaration
class Dispatcher;

class ReadModule: public Module 
{

public:

	//Initialize read module with reference to dispatcher
	ReadModule(Dispatcher& disp);

	~ReadModule();

	//operation method 
	virtual std::string invoke(char* line);

private:

	//A reference to the dispatcher
	Dispatcher& dispatcher;

};


#endif