#ifndef TIFFREADMODULE_H
#define TIFFREADMODULE_H

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "module.h"
#include "preprocessors.h"
#include "tiffState.hpp"
#include <fstream>
#include <map>


class TiffReadModule: public Module 
{

public:

	TiffReadModule(GLubyte (*param)[checkImageWidth][3], TiffState* stateParam);

	//operation method 
	virtual std::string invoke(char* line);

private: //Private Functions

	bool processIFD();

	void processTag(unsigned short tag, unsigned short type, unsigned int count, unsigned int offset);

	void processImageData();

	bool testForReadability();

	void swapShortBytes(unsigned short* twobytes);

	void swapIntBytes( unsigned int* twoshorts);

	void loadTagNames(std::string file = "Tiff_Tag_Names");

private: //Data Members

	bool isBigEndian;

	//Pointer to checkerboard image of window
	GLubyte (*checkPtr)[checkImageWidth][3];

	TiffState* tiffState;

	std::ifstream inFile;

	std::map< int, std::string> tagNames;

};

#endif //TIFFREADMODULE_H