#include "resizeModule.h"
#include <math.h>
#include <cassert>
#include <algorithm>

ResizeModule::ResizeModule(GLubyte (*param)[checkImageWidth][3], TiffState* stateParam): checkPtr(param), tiffState(stateParam), currentFilter(LANCZOS)
{
	
}

std::string ResizeModule::invoke(char* line)
{
	if (!tiffState->fileIsValid)
	{
		return "Error: A previous tiff file has not yet been read.";
	}

	const char* argX = getNextArgument();
	const char* argY = getNextArgument();

	double xScale =  (argX) ? std::stof(argX) : 1.0;
	double yScale =  (argY) ? std::stof(argY) : xScale;

	xScale = fabs(xScale);
	yScale = fabs(yScale);

	int oldWidth = tiffState->ImageWidth;
	int oldHeight = tiffState->ImageLength;

	int newWidth = floor(oldWidth * xScale);
	int newHeight = floor(oldHeight * yScale);

	if (newWidth > 1024 || newHeight > 1024)
	{
		return "Error: Image being scaled is outside the bounds of the array";
	}

	//GRAB THE SOURCE IMAGE
	GLubyte sourceImage[checkImageHeight][checkImageWidth][3];

	for (int i=0; i < oldHeight; ++i)
	{
		for (int j=0; j < oldWidth; ++j)
		{
			sourceImage[i][j][0] = checkPtr[i][j][0];
			sourceImage[i][j][1] = checkPtr[i][j][1];
			sourceImage[i][j][2] = checkPtr[i][j][2];
		}
	}

	//For each row, operate on Row, applying changes to checkPtr
	for (int i= 0; i < oldHeight; ++i)
	{
		//The current row
		GLubyte (*currentRow)[3] = sourceImage[i];

		//Target row
		GLubyte (*targetRow)[3] = checkPtr[i];

		if (newWidth > oldWidth)
		{
			scaleUp(currentRow, targetRow, oldWidth, newWidth);
		}
		else
		{
			scaleDown(currentRow, targetRow, oldWidth, newWidth);
		}


	}

	//UPDATE Source Image
	for (int i=0; i < oldHeight; ++i)
	{
		for (int j=0; j < newWidth; ++j)
		{
			sourceImage[i][j][0] = checkPtr[i][j][0];
			sourceImage[i][j][1] = checkPtr[i][j][1];
			sourceImage[i][j][2] = checkPtr[i][j][2];
		}
	}

	//For each column, operate on Column, applying changes to checkPtr
	for (int i = 0; i < newWidth; ++i)
	{
		//The current column
		GLubyte currentColumn[oldHeight][3];
		for (int j=0; j<oldHeight; ++j)
		{
			currentColumn[j][0] = sourceImage[j][i][0];
			currentColumn[j][1] = sourceImage[j][i][1];
			currentColumn[j][2] = sourceImage[j][i][2];
		}

		//The target column
		GLubyte targetColumn[newHeight][3];
		for (int j=0; j<newHeight; ++j)
		{
			targetColumn[j][0] = 0;
			targetColumn[j][1] = 0;
			targetColumn[j][2] = 0;
		}

		if (newHeight > oldHeight)
		{
			scaleUp(currentColumn, targetColumn, oldHeight, newHeight);
		}
		else
		{
			scaleDown(currentColumn, targetColumn, oldHeight, newHeight);
		}

		//Set CheckPtr column appropriately
		for (int j=0; j < newHeight; ++j)
		{
			checkPtr[j][i][0] = targetColumn[j][0];
			checkPtr[j][i][1] = targetColumn[j][1];
			checkPtr[j][i][2] = targetColumn[j][2];

		}

	}

	//If downsized, fill necessary area with black
	border(oldWidth, oldHeight, newWidth, newHeight);

	//Update Image Width and Height
   	tiffState->ImageWidth = floor(newWidth);
	tiffState->ImageLength = floor(newHeight);
	return "Image Resized by scale factors of x=" + std::to_string(xScale) + " and y=" + std::to_string(yScale);
}

void ResizeModule::scaleUp(GLubyte (*source)[3], GLubyte (*target)[3], double N, double K)
{
	for (int j = 0; j < floor(K); ++j)
	{
		target[j][0] = clamp(reconstructAtPointUp((N/K) * (j + 0.5) - 0.5, source, N, 0));
		target[j][1] = clamp(reconstructAtPointUp((N/K) * (j + 0.5) - 0.5, source, N, 1));
		target[j][2] = clamp(reconstructAtPointUp((N/K) * (j + 0.5) - 0.5, source, N, 2));
	}
}

void ResizeModule::scaleDown(GLubyte (*source)[3], GLubyte (*target)[3], double N, double K)
{
	for (int j = 0; j < floor(K); ++j)
	{
		target[j][0] = clamp(reconstructAtPointDown((N/K) * (j + 0.5) - 0.5, source, N, K, 0));
		target[j][1] = clamp(reconstructAtPointDown((N/K) * (j + 0.5) - 0.5, source, N, K, 1));
		target[j][2] = clamp(reconstructAtPointDown((N/K) * (j + 0.5) - 0.5, source, N, K, 2));
	}
}

double ResizeModule::reconstructAtPointUp(double x, GLubyte (*source)[3], double N, size_t index)
{
	double y = 0.0;
	for (int i=0; i < floor(N); ++i)
	{
		y += source[i][index] * filterSelect(x-i);
	}
	return y;
}

double ResizeModule::reconstructAtPointDown(double x, GLubyte (*source)[3], double N, double K, size_t index)
{
	double y = 0.0;
	for (int i=0; i< floor(N); ++i)
	{
		y += source[i][index] * (K/N) * filterSelect((K/N) * (x-i));
	}
	return y;
}

double ResizeModule::filterSelect(double t)
{
	if (t < 0.0f)
   	{
   		t = -t;
   	}

	switch(currentFilter)
	{
		case LANCZOS:
			return lanczosFilter(t, 1.2);
		case GAUSSIAN:
			return gaussianFilter(t);
		case MITCHELL:
			return mitchellFilter(t, 1.0/3.0, 1.0/3.0);
		case CATMULL:
			return catmullRomFilter(t);
		case HAMMING:
			return hammingFilter(t);
		case TENT:
			return tentFilter(t);
		case BOX:
			return boxFilter(t);
		default:
			return lanczosFilter(t, 1.2);
	}
}

double ResizeModule::lanczosFilter(double t, double a)
{
   	if (t < a)
   	{
   		return sinc(t) * sinc(t / a);
   	}
   	else
   	{
   		return 0.0;
   	}
}

double ResizeModule::gaussianFilter(double t)
{

	double t2 = t/1.25;
	double sigma = 0.7;

	if (t < 1.25)
	{
		return ((1/(sqrt(2 * M_PI * sigma))) * exp(-((t * t)/2 * (sigma * sigma))));
	}
		
	return 0.0f;
}

double ResizeModule::mitchellFilter(double t, double B, double C)
{
	double tSq = t * t;

	if(t < 1.0f)
	{
		t = (((12.0f - 9.0f * B - 6.0f * C) * (t * tSq))
			+ ((-18.0f + 12.0f * B + 6.0f * C) * tSq)
			+ (6.0f - 2.0f * B));

		return (t / 6.0f);
	}
	else if (t < 2.0f)
	{
		t = (((-1.0f * B - 6.0f * C) * (t * tSq))
			+ ((6.0f * B + 30.0f * C) * tSq)
			+ ((-12.0f * B - 48.0f * C) * t)
			+ (8.0f * B + 24.0f * C));

		return (t / 6.0f);
	}

	return (0.0f);
}

double ResizeModule::catmullRomFilter(double t)
{
	return mitchellFilter(t, 0.0f, 0.5f);
}

double ResizeModule::hammingFilter(double t)
{
	double alpha = 0.54;
	double beta = 0.46;
	double width = 1.9;

	
	if (abs(t) < (width-1)/2)
	{
		double sincWeight = sinc(t);
		double windowWeight =  alpha - beta * cos((2 * M_PI * t)/ (width-1));
		return (sincWeight * windowWeight);
	}
	else
	{
		return 0;
	}
	
}

double ResizeModule::tentFilter(double t)
{

	if (t < 1.0f)
	{
		return 1.0f - t;
	}

	return 0.0f;
}

double ResizeModule::boxFilter(double t)
{

	if ((t >= -0.5f) && (t < 0.5f))
	{
		return 1.0f;
	}

	return 0.0f;
}

double ResizeModule::sinc(double x)
{
   x = (x * M_PI);

   if ((x < 0.01) && (x > -0.01))
   {
   		return 1.0;
   }
      
   return sin(x) / x;
}

GLubyte ResizeModule::clamp(double arg)
{
	GLubyte value(arg);
	if (value > 255)
	{
		value = 255;
	}
	return value;
}

void ResizeModule::border(int oldWidth, int oldHeight, int newWidth, int newHeight)
{
	//Lower Right Quadrant
	if (newWidth < oldWidth)
	{
		for (int i=0; i < newHeight; ++i)
		{
			for (int j = newWidth; j < oldWidth; ++j)
			{
				checkPtr[i][j][0] = 0;
				checkPtr[i][j][1] = 0;
				checkPtr[i][j][2] = 0;
			}
		}
	}

	//Upper Left quadrant
	if (newHeight < oldHeight)
	{
		for (int i=0; i < newWidth; ++i)
		{
			for (int j = newHeight; j < oldHeight; ++j)
			{
				checkPtr[j][i][0] = 0;
				checkPtr[j][i][1] = 0;
				checkPtr[j][i][2] = 0;
			}
		}
	}

	//Upper Right quadrant
	if (newHeight < oldHeight && newWidth < oldWidth)
	{
		for (int i=newWidth; i < oldWidth; ++i)
		{
			for (int j = newHeight; j < oldHeight; ++j)
			{
				checkPtr[j][i][0] = 0;
				checkPtr[j][i][1] = 0;
				checkPtr[j][i][2] = 0;
			}
		}
	}
}