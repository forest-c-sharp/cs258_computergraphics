#include "tracer.h"

#include <cmath>
#include <algorithm>

typedef Vec3 RGB;

Tracer::Tracer( GLubyte (*ptr)[checkImageWidth][3] , size_t maxDepth = 4 ): checkPtr(ptr), usePinhole(false),
													 maxRayDepth(maxDepth), backgroundColor(0.8,0.8,1.0), bias(0.01)
{
	//Nothing
}

Tracer::~Tracer()
{
	for (size_t i=0; i<getNumObjects(); ++i)
	{
		removeLastObject();
	}
	for (size_t i=0; i<getNumLights(); ++i)
	{
		removeLastLight();
	}

}

void Tracer::addObject(Object* object)
{
	objectList.push_back(object);
}

void Tracer::removeLastObject()
{
	delete objectList[objectList.size()-1];
	objectList.pop_back();
}

size_t Tracer::getNumObjects()
{
	return objectList.size();
}

void Tracer::addLight(InfinityLight* newLight)
{
	lightList.push_back(newLight);
}

void Tracer::removeLastLight()
{
	delete lightList[lightList.size()-1];
	lightList.pop_back();
}

size_t Tracer::getNumLights()
{
	return lightList.size();
}

void Tracer::setOrtho(OrthoCamera newCam)
{
	orthoCamera = newCam;
}

OrthoCamera& Tracer::getOrtho()
{
	return orthoCamera;
}

void Tracer::setPinhole(PinholeCamera newCam)
{
	pinholeCamera = newCam;
}

PinholeCamera& Tracer::getPinhole()
{
	return pinholeCamera;
}

void Tracer::selectCamera(bool usePin)
{
	usePinhole = usePin;
}

void Tracer::setBGColor(Vec3 col)
{
	backgroundColor = col;
}

//Raytracing main function
void Tracer::traceImage()
{

	if (usePinhole)
	{
		Vec3 vec_a = -(pinholeCamera.getDirection().get());
		Vec3 vec_b = pinholeCamera.getUp().get();

		//Construct orthonormal basis
		Vec3 basis_w( vec_a / vec_a.length());
		Vec3 basis_u( vec_b.cross(basis_w));
		basis_u.normalize();
		Vec3 basis_v( basis_w.cross(basis_u));

		double l(pinholeCamera.getAU());
		double r(pinholeCamera.getBU());
		double b(pinholeCamera.getAV());
		double t(pinholeCamera.getBV());

		size_t width = pinholeCamera.getWidth();
		size_t height = pinholeCamera.getHeight();
		double focal_length = pinholeCamera.getFocalLength();

		Vec3 rayOrigin(pinholeCamera.getOrigin().get());

		for (size_t j=0; j < pinholeCamera.getHeight(); ++j)
		{
			for (size_t i=0; i < pinholeCamera.getWidth(); ++i)
			{
				double u = l + (r - l) * (i + 0.5) / width;
				double v = b + (t - b) * (j + 0.5 ) / height;

				Vec3 rayDir(-basis_w * focal_length + basis_u * u + basis_v * v);

				Ray currentRay(rayOrigin, rayDir, 0);
				Vec3 pixelColor = castRay(currentRay);
				clampRGB(pixelColor);
				checkPtr[j][i][0] = pixelColor.x * 255;
				checkPtr[j][i][1] = pixelColor.y * 255;
				checkPtr[j][i][2] = pixelColor.z * 255;

			}
		}
	}
	else
	{

		Vec3 vec_a = -(orthoCamera.getDirection().get());
		Vec3 vec_b = orthoCamera.getUp().get();

		Vec3 basis_w( vec_a / vec_a.length());
		Vec3 basis_u( vec_b.cross(basis_w));
		basis_u.normalize();
		Vec3 basis_v( basis_w.cross(basis_u));

		double l(pinholeCamera.getAU());
		double r(pinholeCamera.getBU());
		double b(pinholeCamera.getAV());
		double t(pinholeCamera.getBV());

		size_t width = pinholeCamera.getWidth();
		size_t height = pinholeCamera.getHeight();

		Vec3 rayDir(-basis_w);
		for (size_t j=0; j < orthoCamera.getHeight(); ++j)
		{
			for (size_t i=0; i < orthoCamera.getWidth(); ++i)
			{
				double u = l + (r - l) * (i + 0.5) / width;
				double v = b + (t - b) * (j + 0.5 ) / height;
				
				Vec3 rayOrigin(orthoCamera.getOrigin().get() + basis_u * u + basis_v * v);
				Ray currentRay(rayOrigin, rayDir, 0);
				Vec3 pixelColor = castRay(currentRay);
				clampRGB(pixelColor);
				checkPtr[j][i][0] = pixelColor.x * 255;
				checkPtr[j][i][1] = pixelColor.y * 255;
				checkPtr[j][i][2] = pixelColor.z * 255;
			}
		}
	}

}

//Generic raycast function
RGB Tracer::castRay(Ray& ray)
{
	Point pHit;
	Normal nHit;
	Material* surface;

	double newT = -1.0;
	double oldT = std::numeric_limits<double>::max(); //set oldT to highest value for a double

	size_t objID = 0;

	//Go through object List
	for (size_t i=0; i < objectList.size(); ++i)
	{
		Point p;
		Normal n;

		//Test for intersection
		if( objectList[i]->intersect(ray, p, n, newT))
		{
			if (newT >0.0 && newT < oldT) //Closer object
			{
				pHit = p;
				nHit = n;
				surface = objectList[i]->surface;
				oldT = newT;
				objID = i;
			}
		}
	}
	if (newT >0.0) //If an object was hit, evaluate BRDF
	{
		return BRDF (ray, pHit, nHit, surface, objID);
	}
	else
	{
		return backgroundColor;
	}


}

//Bidirectional Reflectance Distribution Function
RGB Tracer::BRDF(Ray& ray, Point& pHit, Normal& nHit, Material* surface, size_t objIndex)
{
	///AMBIENT COLOR/////////////
	RGB ambient(surface->ambientColor);

	////DIFFUSE COLOR///////////(SHADOW RAYS)
	RGB diffuse(0, 0, 0);
	for (size_t i=0; i<lightList.size(); ++i)
	{
		Vec3 start = pHit.get();

		Vec3 lightDir = -(lightList[i]->getDir());

		Ray shadow(pHit.get(), lightDir, 0);

		double theta = nHit.get().dot(lightDir);

		RGB lightCol = castShadowRay(shadow, lightList[i], objIndex);

		diffuse += ( surface->diffuseColor * lightCol * theta );

		//Phong lighting (highlight)
		Vec3 LplusE = lightDir + ray.rayDir.get();
		Vec3 halfVec = (LplusE) * (1 / (LplusE).length());
		double nDotH = nHit.get().dot(halfVec);

		double ks(1);
		if (surface->useSchlick)
		{
			double nAir = 1.000293;
			double nObj = surface->indexOfRefraction;

			double R0 = pow((nAir - nObj)/(nAir + nObj), 2);

			ks = R0 + (1 - R0) * pow(1 - nDotH, 5);

		}

		 RGB phong = (surface->phongColor * lightCol * pow(nDotH,surface->phongPower)) * ks;
		 diffuse += phong;

	}
	
	////REFLECTION RAYS////////////////
	if (ray.rayDepth > maxRayDepth) //Checks if we have exceeded the reflection limit (no more reflections)
	{
		return ( ambient + diffuse );
	}
	
	Vec3 d = ray.rayDir.get();
	Vec3 n = nHit.get();
	size_t newDepth = (ray.rayDepth + 1);

	Vec3 reflectDir = (n - d) * (2 * (n.dot(d)));
	reflectDir.z = -reflectDir.z;

	Vec3 startRay = pHit.get() + reflectDir * bias;

	Ray reflectRay(startRay, reflectDir, newDepth);

	RGB reflection = ( surface->specularColor * castReflectionRay(reflectRay, objIndex));

	return ( ambient + diffuse + reflection );
}

RGB Tracer::castReflectionRay(Ray& ray, size_t currIndex)
{
	Point pHit;
	Normal nHit;
	Material* surface;
	double newT = -1; //Set to sentinel value (-1) will only be -1 until changed by an intersection
	double oldT = std::numeric_limits<double>::max(); //set oldT to highest value for a double

	size_t objID = 0;

	//Go through object List
	for (size_t i=0; i < objectList.size(); ++i)
	{
		Point p; 
		Normal n;

		if (i == currIndex)
		{
			continue;
		}
		
		//Test for intersection
		if( ( objectList[i]->intersect(ray, p, n, newT) ) )
		{
			if (newT > 0 && newT < oldT) //Closer object
			{
				pHit.set(p.get());
				nHit.set(n.get());
				surface = objectList[i]->surface;
				oldT = newT;
				objID = i;
			}
		}
	}
	if (newT > 0) //If an object was hit, evaluate BRDF
	{
		return BRDF (ray, pHit, nHit, surface, objID);
	}
	else
	{
		return Vec3(0,0,0);
	}
}

//Represents a ray aiming at a given light and object permutation, adds contributions and returns value
RGB Tracer::castShadowRay(Ray& ray, InfinityLight* light, size_t currIndex)
{
	Point p; Normal n; double t;

	for (size_t i=0; i < objectList.size(); ++i)
	{	
		if (i == currIndex)
		{
			continue; //Prevent self intersection
		}

		if ( objectList[i]->intersect(ray, p, n, t) )
		{
			if (t > 0.0)
			{
				return RGB(0, 0, 0); //In shadow
			}
		}
	}
	//No intersections, get color of light
	return light->getColor();
}

void Tracer::clampRGB(RGB& rgb)
{
	if (rgb.x < 0.0)
	{
		rgb.x = 0.0;
	}
	if (rgb.x > 1.0)
	{
		rgb.x = 1.0;
	}
	if (rgb.y < 0.0)
	{
		rgb.y = 0.0;
	}
	if (rgb.y > 1.0)
	{
		rgb.y = 1.0;
	}
	if (rgb.z < 0.0)
	{
		rgb.z = 0.0;
	}
	if (rgb.z > 1.0)
	{
		rgb.z = 1.0;
	}

}

