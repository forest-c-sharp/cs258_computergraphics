#ifndef RAYMATH_HPP
#define RAYMATH_HPP

#include <cmath>
#include <iostream>

//3-Dimensional Double Vector Class
class Vec3
{
public:
    double x, y, z;
    Vec3() : x(0.0), y(0.0), z(0.0) {}
    Vec3(double xx) : x(xx), y(xx), z(xx) {}
    Vec3(double xx, double yy, double zz) : x(xx), y(yy), z(zz) {}
    Vec3(const Vec3 &param): x(param.x), y(param.y), z(param.z) {}
    
    //Makes a unit vector
    Vec3& normalize()
    {
        double nor2 = lengthSquared();
        if (nor2 > 0) {
            double invNor = 1 / sqrt(nor2);
            x *= invNor, y *= invNor, z *= invNor;
        }
        return *this;
    }
    //Scalar multiplication and piecewise vector multiplication
    Vec3 operator * (const double &f) const { return Vec3(x * f, y * f, z * f); }
    Vec3 operator * (const Vec3 &v) const { return Vec3(x * v.x, y * v.y, z * v.z); }
    Vec3 operator / (const Vec3 &v) const { return Vec3(x/v.x, y/v.y, z/v.z);}

    //dot product
    double dot(const Vec3 &v) const { return x * v.x + y * v.y + z * v.z; }

    //Cross product
    Vec3 cross(const Vec3 & v) const { 
        Vec3 tmp;
        tmp.x = y * v.z - z * v.y;
        tmp.y = -(x * v.z - z * v.x);
        tmp.z = x * v.y - y * v.x;
        return tmp;
    }

    //Subtraction and addition operators
    Vec3 operator - (const Vec3 &v) const { return Vec3(x - v.x, y - v.y, z - v.z); }
    Vec3 operator + (const Vec3 &v) const { return Vec3(x + v.x, y + v.y, z + v.z); }

    //add-assign and mult-assign operator
    Vec3& operator = (const Vec3 & v) { x = v.x, y = v.y, z = v.z; return *this; }
    Vec3& operator += (const Vec3 &v) { x += v.x, y += v.y, z += v.z; return *this; }
    Vec3& operator *= (const Vec3 &v) { x *= v.x, y *= v.y, z *= v.z; return *this; }

    //Negation operator
    Vec3 operator - () const { return Vec3(-x, -y, -z); }

    //Lengthsquared and length(norm)
    double lengthSquared() const { return x * x + y * y + z * z; }
    double length() const { return sqrt(lengthSquared()); }

    //Output stream operator
    friend std::ostream & operator << (std::ostream &os, const Vec3 &v)
    {
        os << "[" << v.x << " " << v.y << " " << v.z << "]";
        return os;
    }
};

//Normal class (Must have length of 1)
class Normal {
public:

    Normal():data(1,0,0) {}
    Normal(double x, double y, double z):data(x,y,z) {
            data.normalize();
    }
    Normal(Vec3 v):data(v) {
            data.normalize();
    }
    Normal(const Normal &n):data(n.data) {

    }

    void set(Vec3 v)
    {
        data = v;
        data.normalize();
    }

    const Vec3& get() const
    {
        return data;
    }
private:

    Vec3 data;


};

//Point class
class Point {
public:

    Point(): data(0, 0, 0) {}
    Point(double x, double y, double z): data(x,y,z) { }
    Point(Vec3 v): data(v) { }
    Point(const Point& p):data(p.data) { }

    void set(Vec3 v)
    {
        data = v;
    }

    const Vec3& get() const
    {
        return data;
    }
private:

    Vec3 data;

};

//Ray class (defines an origin point, a vector direction, and a ray depth)
class Ray {
public:

    Ray(Point ori, Normal dir, size_t depth=0): rayOrigin(ori), rayDir(dir), rayDepth(depth) {}
    Ray(const Ray& param): Ray(param.rayOrigin, param.rayDir, param.rayDepth) {}

    const Point rayOrigin;
    const Normal rayDir;
    const size_t rayDepth;


};


#endif