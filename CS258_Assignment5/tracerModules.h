#ifndef TRACERMODULES_H
#define TRACERMODULES_H

#include "module.h"
#include "tracer.h"

//Reshapes screen
class ScreenModule: public Module {
public:

	ScreenModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;

};

//Orthographic Projection camera
class OrthoModule: public Module {
public:

OrthoModule(Tracer* ptr);

std::string invoke(char* line);


private:

	Tracer* tracerPtr;

};

//Pinhole Perspective Camera
class CameraModule: public Module {
public:

	CameraModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;
};

//Changes BG Color
class BackgroundModule: public Module {
public:

	BackgroundModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;

};

//Sphere
class SphereModule: public Module {
public:

	SphereModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;

};

//Triangle
class TriangleModule: public Module {
public:

	TriangleModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;

};

//Bounding Box Axis Aligned
class BoxModule: public Module {
public:

	BoxModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;
};

//Plane
class PlaneModule: public Module {
public:

	PlaneModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;
};

//Infinity Light
class ILightModule: public Module {
public:

	ILightModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;
};

//Clears screen, scene (objects and lights)
class ClearModule: public Module {
public:

	ClearModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;
};

//Traces current object,light and camera setup
class TraceModule: public Module {
public:

	TraceModule(Tracer* ptr);

	std::string invoke(char* line);


private:

	Tracer* tracerPtr;
};





#endif