#include "rayLights.h"

	InfinityLight::InfinityLight(): color(1,1,1), direction(0, -1, -0.1)
	{
		//Nothing
	}

	InfinityLight::InfinityLight(Vec3 c, Vec3 d): color(c), direction(d)
	{
		//Nothing
	}

	const Vec3& InfinityLight::getColor() const
	{
		return color;
	}
	const Vec3& InfinityLight::getDir() const
	{
		return direction.get();
	}