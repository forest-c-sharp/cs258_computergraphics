#ifndef TRACER_H
#define TRACER_H

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <vector>
#include "preprocessors.h"
#include "rayObjects.h"
#include "rayCameras.h"
#include "rayMath.hpp"
#include "rayLights.h"

class Tracer 
{

	typedef Vec3 RGB;

public:

	//Takes in the image to be written and a maxDepth for reflection rays
	Tracer( GLubyte (*ptr)[checkImageWidth][3], size_t maxDepth);

	//No default constructor
	Tracer() = delete;

	~Tracer();

	//adds and object to the object list
	void addObject(Object* newObj);

	void removeLastObject();

	size_t getNumObjects();

	//Adds a light to the scene
	void addLight(InfinityLight* newLight);

	void removeLastLight();

	size_t getNumLights();

	//Sets orthographic camera
	void setOrtho(OrthoCamera newCam);

	//Returns ortho camera to modify
	OrthoCamera& getOrtho();

	//Sets the pinhole camera
	void setPinhole(PinholeCamera newCam);

	//Returns the pinholeCamera to modify
	PinholeCamera& getPinhole();

	//Selects which camera to use in TraceImage function
	void selectCamera(bool usePin);

	//Sets Background color (if values greater than 1 or less than 0, then clamp)
	void setBGColor(Vec3 color);

	//Traces the image onto the viewport (the "for-each-pixel" function)
	void traceImage();

private: //Functions

	//Casts primary ray (to get nearest object intersection) and calls BRDF if hit, otherwise return BG color
	RGB castRay(Ray& ray);

	//Evaluates BRDF at pHit with nHit(calling additional rayCasts as appropriate)
	RGB BRDF(Ray& ray, Point& pHit, Normal& nHit, Material* surface, size_t index);

	//Reflection Ray
	RGB castReflectionRay(Ray& ray, size_t currIndex);

	//Diffuse component (Either reaches a light source(iterates over all lights) or terminates in shadow returning [0,0,0])
	RGB castShadowRay(Ray& ray, InfinityLight* light, size_t currIndex);

	//Simple clamp function
	void clampRGB(RGB& rgb);




private: //Data members

	//Pointer to checkerboard image of window
	GLubyte (*checkPtr)[checkImageWidth][3];

	std::vector< Object* > objectList;

	std::vector< InfinityLight* > lightList;

	OrthoCamera orthoCamera;

	PinholeCamera pinholeCamera;

	bool usePinhole;

	size_t maxRayDepth;

	RGB backgroundColor;

	double bias; //To move points away from their surface

};

#endif