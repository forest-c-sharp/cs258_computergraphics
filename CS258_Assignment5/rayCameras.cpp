#include "rayCameras.h"

/////////////////CAMERA///////////////////////

Camera::Camera():camOrigin(Vec3(0,0,0)), camDir(Vec3(0,0,-1)), camUp(Vec3(0,1,0)), screenWidth(512), screenHeight(512),
				au(-256), av(-256), bu(256), bv(256)
{
	//nothing
}

Camera::Camera(Point o, Normal dir, Normal up, int width, int height, int AU, int AV, int BU, int BV):
				camOrigin(o), camDir(dir), camUp(up), screenWidth(width), screenHeight(height),
				au(AU), av(AV), bu(BU), bv(BV)
{
	//nothing
}

Camera::~Camera()
{
	//Nothing
}

void Camera::setOrigin(Point newOri)
{
	camOrigin.set(newOri.get());
}
const Point& Camera::getOrigin() const
{
	return camOrigin;
}

void Camera::setDirection(Normal newDir)
{
	camDir.set(newDir.get());
}
const Normal& Camera::getDirection() const
{
	return camDir;
}

void Camera::setUp(Normal newUp)
{
	camUp.set(newUp.get());
}
const Normal& Camera::getUp() const
{
	return camUp;
}

void Camera::setWidth(int newWidth)
{
	screenWidth = newWidth;
}
const int& Camera::getWidth() const
{
	return screenWidth;
}

void Camera::setHeight(int newHeight)
{
	screenHeight = newHeight;
}
const int& Camera::getHeight() const
{
	return screenHeight;
}

void Camera::setDimensions(int AU, int AV, int BU, int BV)
{
	au = AU;
	av = AV;
	bu = BU;
	bv = BV;
}
const int& Camera::getAU() const
{
	return au;
}
const int& Camera::getAV() const
{
	return av;
}
const int& Camera::getBU() const
{
	return bu;
}
const int& Camera::getBV() const
{
	return bv;
}

/////////////////ORTHO///////////////////////


OrthoCamera::OrthoCamera(): Camera()
{
	//Nothing
}

OrthoCamera::OrthoCamera(const OrthoCamera& cam):Camera()
{
	//nothing
}

OrthoCamera& OrthoCamera::operator = (const OrthoCamera& rhs)
{
	if (this != &rhs)
	{
		OrthoCamera tmp(rhs);

		std::swap(*this, tmp);
	}
	return *this;
}


/////////////////PINHOLE///////////////////////

PinholeCamera::PinholeCamera(): Camera(), focalLength(1.04)
{
	//Nothing
}

PinholeCamera::PinholeCamera(const PinholeCamera& cam):PinholeCamera(cam.camOrigin, cam.camDir, cam.camUp, cam.screenWidth, cam.screenHeight,
																cam.focalLength, cam.au, cam.av, cam.bu, cam.bv)
{
	//Nothing
}

PinholeCamera::PinholeCamera(	Point o, Normal dir, Normal up, 
					int width, int height, double s,
					int au, int av, int bu, int bv): 
					Camera(o, dir, up, width, height, au, av, bu, bv), focalLength(s)
{
	//Nothing
}

PinholeCamera& PinholeCamera::operator = (const PinholeCamera rhs)
{
	if (this != &rhs)
	{
		camOrigin = rhs.camOrigin;
		camDir = rhs.camDir;
		camUp = rhs.camUp;
		screenWidth = rhs.screenWidth;
		screenHeight = rhs.screenHeight;
		au = rhs.au;
		av = rhs.av;
		bu = rhs.bu;
		bv = rhs.bv;
		focalLength = rhs.focalLength;

	}
	return *this;
}

void PinholeCamera::setFocalLength(double f)
{
	focalLength = f;
}
const double PinholeCamera::getFocalLength() const
{
	return focalLength;
}