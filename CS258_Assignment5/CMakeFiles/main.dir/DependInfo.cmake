# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/colorModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/colorModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/dispatcher.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/dispatcher.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/drawModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/drawModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/main.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/main.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/module.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/module.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/moveModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/moveModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/rayCameras.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/rayCameras.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/rayLights.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/rayLights.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/rayObjects.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/rayObjects.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/readModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/readModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/resizeModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/resizeModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/selectModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/selectModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/tiffReadModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/tiffReadModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/tiffStatModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/tiffStatModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/tiffWriteModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/tiffWriteModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/tracer.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/tracer.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/tracerModules.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/tracerModules.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/zoomModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment5/CMakeFiles/main.dir/zoomModule.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
