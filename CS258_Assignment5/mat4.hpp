#ifndef MAT4_HPP
#define MAT4_HPP

class Mat4 
{

public:

	//Def ctor: identity matrix
	Mat4()
	{
		for (int i=0; i<4; ++i)
		{
			for (int j=0; j<4; ++j)
			{
				if (i ==j)
				{
					matrixData[i][j] = 1.0;
				}
				else
				{
					matrixData[i][j] = 0.0;
				}
			}
		}
	}

	//Copy Ctor
	Mat4(const Mat4 &rhs)
	{
		for (int i=0; i<4; ++i) //columns
		{
			for (int j=0; j<4; ++j) //rows
			{
				matrixData[i][j] = rhs.matrixData[i][j];
			}
		}
	}

	//Full broken-out constructor
	Mat4(	double x11, double x12, double x13, double x14,
			double x21, double x22, double x23, double x24,
			double x31, double x32, double x33, double x34,
			double x41, double x42, double x43, double x44):
	matrixData{	{x11, x21, x31, x41},
				{x12, x22, x32, x42},
				{x13, x23, x33, x43},
				{x14, x24, x34, x44}}
	{
		/*matrixData[0][0] = x11;
		matrixData[1][0] = x12;
		matrixData[2][0] = x13;
		matrixData[3][0] = x14;
		matrixData[0][1] = x21;
		matrixData[1][1] = x22;
		matrixData[2][1] = x23;
		matrixData[3][1] = x24;
		matrixData[0][2] = x31;
		matrixData[1][2] = x32;
		matrixData[2][2] = x33;
		matrixData[3][2] = x34;
		matrixData[0][3] = x41;
		matrixData[1][3] = x42;
		matrixData[2][3] = x43;
		matrixData[3][3] = x44;*/
	}

	//Assignment operator
	Mat4 operator=(const Mat4& rhs)
	{
		if (this != &rhs)
		{
			Mat4 newMat(rhs);
			return newMat;
		}
		
		return *this;
	}

	//Matrix multiplication
	Mat4 operator*(const Mat4& rhs)
	{
		Mat4 newMat;

		for (int i=0; i<4; ++i) //Column of new mat
		{
			for (int j=0; j<4; ++j) //Row of new mat
			{
				newMat.matrixData[i][j] = 	matrixData[0][j] * rhs.matrixData[i][0] +
											matrixData[1][j] * rhs.matrixData[i][1] +
											matrixData[2][j] * rhs.matrixData[i][2] +
											matrixData[3][j] * rhs.matrixData[i][3];
			}
		}

		return newMat;
	}

	//Matrix addition
	Mat4 operator+(const Mat4& rhs)
	{
		Mat4 newMat;
		for (int i=0; i<4; ++i)
		{
			for (int j=0; j<4; ++j)
			{
				newMat.matrixData[i][j] = matrixData[i][j] + rhs.matrixData[i][j];
			}
		}
		return newMat;
	}


private:

	//Data for matrix: [columns][rows]
	double matrixData[4][4];


};


#endif