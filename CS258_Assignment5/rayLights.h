#ifndef RAYLIGHTS_H
#define RAYLIGHTS_H

#include "rayMath.hpp"

class InfinityLight {

public:

	InfinityLight();

	InfinityLight(Vec3 c, Vec3 d);

	const Vec3& getColor() const;
	
	const Vec3& getDir() const;

private:

	Vec3 color;

	Normal direction;

};



#endif