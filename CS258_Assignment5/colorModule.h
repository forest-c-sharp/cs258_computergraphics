#ifndef COLORMODULE_H
#define COLORMODULE_H

#include "module.h"

class ColorModule: public Module
{
public:

	ColorModule();

	~ColorModule();

	std::string invoke(char* line);


};


#endif //COLORMODULE_H
