#ifndef OBJECTS_H
#define OBJECTS_H

#include "rayMath.hpp"

//Defines the surface properties of an object
class Material {
public:

	Material();

	Material(Vec3 amb, Vec3 diff, Vec3 spec,Vec3 phong, double power, bool schlick, double iof);

	Vec3 ambientColor; //Colors the object independent of lighting

	Vec3 diffuseColor; //determines diffuse color if shadow ray reaches Ilight

	Vec3 specularColor; //Specular reflection color

	Vec3 phongColor; //For phong highlight

	double phongPower; //for POW calc

	bool useSchlick; //Whether or not to use Schlick approximation on a given object

	double indexOfRefraction; //For schlick approximation


};

//Object abstract base class
class Object {
public:

	Object(Material* myMat);

	virtual ~Object() = 0;

	virtual bool intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit) = 0;

	Material* surface; //Surface properties of a given object

};

//Sphere primitive class
class Sphere: public Object {
public:

	Sphere(Material* myMat, Point c, double r);

	virtual bool intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit);

private:

	Point center;

	double radius;

};

//Triangle primitive class
class Triangle: public Object {
public:

	Triangle(Material* myMat, Point a1, Point b1, Point c1);

	virtual bool intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit);

private:

	Point a, b, c; //Triangle vertices

};

//Bounding Box primitive class
class BBox: public Object {
public:

	BBox(Material* myMat, Point n, Point f);

	virtual bool intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit);

private:

	Point near, far; //Nearest and Farthest corners of BBox

	bool compareDoubles(double a, double b);

};

//Plane primitive class
class Plane: public Object {
public:

	Plane(Material* myMat, Normal n, Point p);

	virtual bool intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit);

private:

	Normal norm;

	Point point;

};




#endif