#include "tracerModules.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "rayMath.hpp"
#include "rayObjects.h"
#include "rayLights.h"

//SCREEN MODULE

ScreenModule::ScreenModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string ScreenModule::invoke(char* line)
{
	const char* argNX = getNextArgument();
	const char* argNY = getNextArgument();

	int NX =  (argNX) ? std::stoi(argNX) : 512;
	int NY =  (argNY) ? std::stof(argNY) : 512;

	(tracerPtr->getOrtho()).setWidth(NX);
	(tracerPtr->getOrtho()).setHeight(NY);

	(tracerPtr->getPinhole()).setWidth(NX);
	(tracerPtr->getPinhole()).setHeight(NY);

	glutReshapeWindow(NX,NY);

	return "Screen Width and Height Set";

}

//OrthoModule
OrthoModule::OrthoModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string OrthoModule::invoke(char* line)
{
	tracerPtr->selectCamera(false);

	return "Using Orthographic Camera";
}

//CameraModule
CameraModule::CameraModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string CameraModule::invoke(char* line)
{
	tracerPtr->selectCamera(true);

	//Eye
	const char* argEX = getNextArgument();
	const char* argEY = getNextArgument();
	const char* argEZ = getNextArgument();

	double EX =  (argEX) ? std::stof(argEX) : 0.0;
	double EY =  (argEY) ? std::stof(argEY) : 0.0;
	double EZ =  (argEZ) ? std::stof(argEZ) : 0.0;

	//Gaze
	const char* argGX = getNextArgument();
	const char* argGY = getNextArgument();
	const char* argGZ = getNextArgument();

	double GX =  (argGX) ? std::stof(argGX) : 0.0;
	double GY =  (argGY) ? std::stof(argGY) : 0.0;
	double GZ =  (argGZ) ? std::stof(argGZ) : -1.0;

	//Up
	const char* argUX = getNextArgument();
	const char* argUY = getNextArgument();
	const char* argUZ = getNextArgument();

	double UX =  (argUX) ? std::stof(argUX) : 0.0;
	double UY =  (argUY) ? std::stof(argUY) : 1.0;
	double UZ =  (argUZ) ? std::stof(argUZ) : 0.0;

	//Focal Length
	const char* argS = getNextArgument();

	double S =  (argS) ? std::stof(argS) : 1.0;

	//Screen Dimensions
	const char* argAU = getNextArgument();
	const char* argAV = getNextArgument();
	const char* argBU = getNextArgument();
	const char* argBV = getNextArgument();

	double AU =  (argAU) ? std::stof(argAU) : -1.0;
	double AV =  (argAV) ? std::stof(argAV) : -1.0;
	double BU =  (argBU) ? std::stof(argBU) : 1.0;
	double BV =  (argBV) ? std::stof(argBV) : 1.0;

	//Unchanged
	size_t width = (tracerPtr->getPinhole()).getWidth();
	size_t height = (tracerPtr->getPinhole()).getHeight();

	PinholeCamera newCam(Vec3(EX,EY,EZ), Vec3(GX,GY,GZ), Vec3(UX,UY,UZ), width, height, S, AU, AV, BU, BV);

	tracerPtr->setPinhole(newCam);

	return "Pinhole Camera Set";
	
}

//BackgroundModule
BackgroundModule::BackgroundModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string BackgroundModule::invoke(char* line)
{
	//Background Color
	const char* argAR = getNextArgument();
	const char* argAG = getNextArgument();
	const char* argAB = getNextArgument();

	double AR =  (argAR) ? std::stof(argAR) : 0.0;
	double AG =  (argAG) ? std::stof(argAG) : 0.0;
	double AB =  (argAB) ? std::stof(argAB) : 0.0;

	tracerPtr->setBGColor(Vec3(AR,AG,AB));

	return "Background Color Set";
}

//SphereModule
SphereModule::SphereModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string SphereModule::invoke(char* line)
{
	//Radius 
	const char* argR = getNextArgument();

	double R =  (argR) ? std::stof(argR) : 50.0;

	//Center
	const char* argCX = getNextArgument();
	const char* argCY = getNextArgument();
	const char* argCZ = getNextArgument();

	double CX =  (argCX) ? std::stof(argCX) : 0.0;
	double CY =  (argCY) ? std::stof(argCY) : 0.0;
	double CZ =  (argCZ) ? std::stof(argCZ) : -200.0;

	//Ambient Lighting
	const char* argAR = getNextArgument();
	const char* argAG = getNextArgument();
	const char* argAB = getNextArgument();

	double AR =  (argAR) ? std::stof(argAR) : 0.0;
	double AG =  (argAG) ? std::stof(argAG) : 0.0;
	double AB =  (argAB) ? std::stof(argAB) : 0.0;

	//Diffuse Lighting
	const char* argRR = getNextArgument();
	const char* argRG = getNextArgument();
	const char* argRB = getNextArgument();

	double RR =  (argRR) ? std::stof(argRR) : 1.0;
	double RG =  (argRG) ? std::stof(argRG) : 0.0;
	double RB =  (argRB) ? std::stof(argRB) : 0.0;

	//Specular Reflection
	const char* argSR = getNextArgument();
	const char* argSG = getNextArgument();
	const char* argSB = getNextArgument();

	double SR =  (argSR) ? std::stof(argSR) : 0.2;
	double SG =  (argSG) ? std::stof(argSG) : 0.2;
	double SB =  (argSB) ? std::stof(argSB) : 0.2;

	//Phong Lighting
	const char* argPR = getNextArgument();
	const char* argPG = getNextArgument();
	const char* argPB = getNextArgument();

	double PR =  (argPR) ? std::stof(argPR) : 1.0;
	double PG =  (argPG) ? std::stof(argPG) : 1.0;
	double PB =  (argPB) ? std::stof(argPB) : 1.0;

	//Phong Power
	const char* argPP = getNextArgument();

	double PP =  (argPP) ? std::stof(argPP) : 0.0;

	//UseSchlick
	const char* argSchlick = getNextArgument();

	bool SCHLICK =  (argSchlick) ? std::stoi(argSchlick) : 0.0;

	//Index of Refraction
	const char* argIoF = getNextArgument();

	double IoF =  (argIoF) ? std::stof(argIoF) : 1.52;

	Material* material = new Material(Vec3(AR,AG,AB), Vec3(RR,RG,RB), Vec3(SR,SG,SB), Vec3(PR,PG,PB), PP, SCHLICK, IoF);
	Sphere* sphere = new Sphere(material, Vec3(CX,CY,CZ), R);
	tracerPtr->addObject(sphere);

	return "Sphere added to Scene";
}

//TriangleModule
TriangleModule::TriangleModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string TriangleModule::invoke(char* line)
{
	//Vertex A
	const char* argAX = getNextArgument();
	const char* argAY = getNextArgument();
	const char* argAZ = getNextArgument();

	double AX =  (argAX) ? std::stof(argAX) : 50.0;
	double AY =  (argAY) ? std::stof(argAY) : 50.0;
	double AZ =  (argAZ) ? std::stof(argAZ) : -50.0;

	//Vertex B
	const char* argBX = getNextArgument();
	const char* argBY = getNextArgument();
	const char* argBZ = getNextArgument();

	double BX =  (argBX) ? std::stof(argBX) : 100.0;
	double BY =  (argBY) ? std::stof(argBY) : 55.0;
	double BZ =  (argBZ) ? std::stof(argBZ) : -50.0;

	//Vertex C
	const char* argCX = getNextArgument();
	const char* argCY = getNextArgument();
	const char* argCZ = getNextArgument();

	double CX =  (argCX) ? std::stof(argCX) : 60.0;
	double CY =  (argCY) ? std::stof(argCY) : 105.0;
	double CZ =  (argCZ) ? std::stof(argCZ) : -50.0;

	//Ambient Lighting
	const char* argAR = getNextArgument();
	const char* argAG = getNextArgument();
	const char* argAB = getNextArgument();

	double AR =  (argAR) ? std::stof(argAR) : 0.0;
	double AG =  (argAG) ? std::stof(argAG) : 0.0;
	double AB =  (argAB) ? std::stof(argAB) : 0.0;

	//Diffuse Lighting
	const char* argRR = getNextArgument();
	const char* argRG = getNextArgument();
	const char* argRB = getNextArgument();

	double RR =  (argRR) ? std::stof(argRR) : 1.0;
	double RG =  (argRG) ? std::stof(argRG) : 0.0;
	double RB =  (argRB) ? std::stof(argRB) : 0.0;

	//Specular Reflection
	const char* argSR = getNextArgument();
	const char* argSG = getNextArgument();
	const char* argSB = getNextArgument();

	double SR =  (argSR) ? std::stof(argSR) : 0.5;
	double SG =  (argSG) ? std::stof(argSG) : 0.5;
	double SB =  (argSB) ? std::stof(argSB) : 0.5;

	//Phong Lighting
	const char* argPR = getNextArgument();
	const char* argPG = getNextArgument();
	const char* argPB = getNextArgument();

	double PR =  (argPR) ? std::stof(argPR) : 1.0;
	double PG =  (argPG) ? std::stof(argPG) : 1.0;
	double PB =  (argPB) ? std::stof(argPB) : 1.0;

	//Phong Power
	const char* argPP = getNextArgument();

	double PP =  (argPP) ? std::stof(argPP) : 0.0;

	//UseSchlick
	const char* argSchlick = getNextArgument();

	bool SCHLICK =  (argSchlick) ? std::stoi(argSchlick) : 0.0;

	//Index of Refraction
	const char* argIoF = getNextArgument();

	double IoF =  (argIoF) ? std::stof(argIoF) : 1.52;

	Material* material = new Material(Vec3(AR,AG,AB), Vec3(RR,RG,RB), Vec3(SR,SG,SB), Vec3(PR,PG,PB), PP, SCHLICK, IoF);
	Triangle* tri = new Triangle(material, Vec3(AX,AY,AZ), Vec3(BX,BY,BZ), Vec3(CX,CY,CZ));
	tracerPtr->addObject(tri);

	return "Triangle added to Scene";
}

//BoxModule
BoxModule::BoxModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string BoxModule::invoke(char* line)
{
	//Box Near
	const char* argNX = getNextArgument();
	const char* argNY = getNextArgument();
	const char* argNZ = getNextArgument();

	double NX =  (argNX) ? std::stof(argNX) : 200.0;
	double NY =  (argNY) ? std::stof(argNY) : 100.0;
	double NZ =  (argNZ) ? std::stof(argNZ) : -5.0;

	//Box Far
	const char* argFX = getNextArgument();
	const char* argFY = getNextArgument();
	const char* argFZ = getNextArgument();

	double FX =  (argFX) ? std::stof(argFX) : 400.0;
	double FY =  (argFY) ? std::stof(argFY) : 300.0;
	double FZ =  (argFZ) ? std::stof(argFZ) : -15.0;

	//Ambient Lighting
	const char* argAR = getNextArgument();
	const char* argAG = getNextArgument();
	const char* argAB = getNextArgument();

	double AR =  (argAR) ? std::stof(argAR) : 0.0;
	double AG =  (argAG) ? std::stof(argAG) : 0.0;
	double AB =  (argAB) ? std::stof(argAB) : 0.0;

	//Diffuse Lighting
	const char* argRR = getNextArgument();
	const char* argRG = getNextArgument();
	const char* argRB = getNextArgument();

	double RR =  (argRR) ? std::stof(argRR) : 1.0;
	double RG =  (argRG) ? std::stof(argRG) : 0.0;
	double RB =  (argRB) ? std::stof(argRB) : 0.0;

	//Specular Reflection
	const char* argSR = getNextArgument();
	const char* argSG = getNextArgument();
	const char* argSB = getNextArgument();

	double SR =  (argSR) ? std::stof(argSR) : 0.5;
	double SG =  (argSG) ? std::stof(argSG) : 0.5;
	double SB =  (argSB) ? std::stof(argSB) : 0.5;

	//Phong Lighting
	const char* argPR = getNextArgument();
	const char* argPG = getNextArgument();
	const char* argPB = getNextArgument();

	double PR =  (argPR) ? std::stof(argPR) : 1.0;
	double PG =  (argPG) ? std::stof(argPG) : 1.0;
	double PB =  (argPB) ? std::stof(argPB) : 1.0;

	//Phong Power
	const char* argPP = getNextArgument();

	double PP =  (argPP) ? std::stof(argPP) : 0.0;

	//UseSchlick
	const char* argSchlick = getNextArgument();

	bool SCHLICK =  (argSchlick) ? std::stoi(argSchlick) : 0.0;

	//Index of Refraction
	const char* argIoF = getNextArgument();

	double IoF =  (argIoF) ? std::stof(argIoF) : 1.52;

	Material* material = new Material(Vec3(AR,AG,AB), Vec3(RR,RG,RB), Vec3(SR,SG,SB), Vec3(PR,PG,PB), PP, SCHLICK, IoF);
	BBox* box = new BBox(material, Vec3(NX,NY,NZ), Vec3(FX,FY,FZ));
	tracerPtr->addObject(box);

	return "Box added to Scene";
	
}

//PlaneModule
PlaneModule::PlaneModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string PlaneModule::invoke(char* line)
{
	//Plane Normal
	const char* argNX = getNextArgument();
	const char* argNY = getNextArgument();
	const char* argNZ = getNextArgument();

	double NX =  (argNX) ? std::stof(argNX) : 0.0;
	double NY =  (argNY) ? std::stof(argNY) : 1.0;
	double NZ =  (argNZ) ? std::stof(argNZ) : 0.0;

	//Plane Point
	const char* argPX = getNextArgument();
	const char* argPY = getNextArgument();
	const char* argPZ = getNextArgument();

	double PX =  (argPX) ? std::stof(argPX) : 0.0;
	double PY =  (argPY) ? std::stof(argPY) : -100.0;
	double PZ =  (argPZ) ? std::stof(argPZ) : 0.0;

	//Ambient Lighting
	const char* argAR = getNextArgument();
	const char* argAG = getNextArgument();
	const char* argAB = getNextArgument();

	double AR =  (argAR) ? std::stof(argAR) : 0.0;
	double AG =  (argAG) ? std::stof(argAG) : 0.0;
	double AB =  (argAB) ? std::stof(argAB) : 0.0;

	//Diffuse Lighting
	const char* argRR = getNextArgument();
	const char* argRG = getNextArgument();
	const char* argRB = getNextArgument();

	double RR =  (argRR) ? std::stof(argRR) : 1.0;
	double RG =  (argRG) ? std::stof(argRG) : 0.0;
	double RB =  (argRB) ? std::stof(argRB) : 0.0;

	//Specular Reflection
	const char* argSR = getNextArgument();
	const char* argSG = getNextArgument();
	const char* argSB = getNextArgument();

	double SR =  (argSR) ? std::stof(argSR) : 1.0;
	double SG =  (argSG) ? std::stof(argSG) : 1.0;
	double SB =  (argSB) ? std::stof(argSB) : 1.0;

	//Phong Lighting
	const char* argPR = getNextArgument();
	const char* argPG = getNextArgument();
	const char* argPB = getNextArgument();

	double PR =  (argPR) ? std::stof(argPR) : 1.0;
	double PG =  (argPG) ? std::stof(argPG) : 1.0;
	double PB =  (argPB) ? std::stof(argPB) : 1.0;

	//Phong Power
	const char* argPP = getNextArgument();

	double PP =  (argPP) ? std::stof(argPP) : 0.0;

	//UseSchlick
	const char* argSchlick = getNextArgument();

	bool SCHLICK =  (argSchlick) ? std::stoi(argSchlick) : 0.0;

	//Index of Refraction
	const char* argIoF = getNextArgument();

	double IoF =  (argIoF) ? std::stof(argIoF) : 1.52;

	Material* material = new Material(Vec3(AR,AG,AB), Vec3(RR,RG,RB), Vec3(SR,SG,SB), Vec3(PR,PG,PB), PP, SCHLICK, IoF);
	Plane* plane = new Plane(material, Vec3(NX,-NY,NZ), Vec3(PX,PY,PZ));
	tracerPtr->addObject(plane);

	return "Plane added to Scene";
	
}

//ILightModule
ILightModule::ILightModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string ILightModule::invoke(char* line)
{
	const char* argLR = getNextArgument();
	const char* argLG = getNextArgument();
	const char* argLB = getNextArgument();

	double LR =  (argLR) ? std::stof(argLR) : 0.5;
	double LB =  (argLG) ? std::stof(argLG) : 0.5;
	double LG =  (argLB) ? std::stof(argLB) : 0.5;

	const char* argDX = getNextArgument();
	const char* argDY = getNextArgument();
	const char* argDZ = getNextArgument();

	double DX =  (argDX) ? std::stof(argDX) : 0.0;
	double DY =  (argDY) ? std::stof(argDY) : -1.0;
	double DZ =  (argDZ) ? std::stof(argDZ) : 0.0;

	DZ *= -1;

	InfinityLight* light = new InfinityLight(Vec3(LR,LB,LG), Vec3(DX,DY,DZ));
	tracerPtr->addLight(light);

	return "Light added to Scene";


}

//ClearModule
ClearModule::ClearModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string ClearModule::invoke(char* line)
{
	size_t numObjects = tracerPtr->getNumObjects();
	for (size_t i=0; i<numObjects; ++i)
	{
		tracerPtr->removeLastObject();
	}
	size_t numLights = tracerPtr->getNumLights();
	for (size_t i=0; i<numLights; ++i)
	{
		tracerPtr->removeLastLight();
	}

	tracerPtr->traceImage();

	return "Screen cleared, object and light lists reset.";
}

//TraceModule
TraceModule::TraceModule(Tracer* ptr): tracerPtr(ptr)
{
	//Nothing
}

std::string TraceModule::invoke(char* line)
{
	tracerPtr->traceImage();

	return "Image traced on screen.";
}