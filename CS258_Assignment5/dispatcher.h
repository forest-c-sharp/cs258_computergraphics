//FOREST C SHARP

#ifndef DISPATCHER_H
#define DISPATCHER_H

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <string>
#include <map>

#include "module.h"
#include "preprocessors.h"
#include "tiffState.hpp"
#include "tracer.h"

/** 
 * 	Dispatcher class responsible for maintaining current input 
 * 		and delegating that input to the appropriate work modules
 */
class Dispatcher 
{

public:

	//Initializes all work modules
	Dispatcher(GLubyte (*param)[checkImageWidth][3]);

	~Dispatcher();

	//Sets the current line, sends command to dispatchCommand function
	void processCommand(char* line);

private:

	//Pointer to checkerboard image of window
	GLubyte (*checkPtr)[checkImageWidth][3];

	char* currLine;

	//A map of key-value pairs pointing to each module
	std::map<std::string, Module*> moduleMap;

	//State of lastTiff read
	TiffState tiffState;

	//Tracer Class (controls all aspects of raytracer)
	Tracer tracer;

};


#endif //DISPATCHER_H