#include "rayObjects.h"
#include <cmath>
#include <algorithm>
#include <iostream>

///////////////////////////////////MATERIAL////////////////////////////////////////////////////////////////

Material::Material(): 	ambientColor(Vec3(0.2, 0.2, 0.2)), diffuseColor(Vec3(1.0, 0, 0)),
						specularColor(Vec3(1.0, 1.0, 1.0)), phongColor(Vec3(1.0, 1.0, 1.0)), phongPower(48.0),
						useSchlick(true), indexOfRefraction(1.52)
{
	//Nothing left to do
}

Material::Material(Vec3 amb, Vec3 diff, Vec3 spec, Vec3 phong, double power, bool schlick, double iof):
					ambientColor(amb), diffuseColor(diff), specularColor(spec), phongColor(phong), phongPower(power),
					useSchlick(schlick), indexOfRefraction(iof)
{
	//Nothing left to do
}

////////////////////////////////////OBJECT////////////////////////////////////////////////////////////////

Object::Object(Material* myMat): surface(myMat)
{
	//Nothing left to do
}

Object::~Object() 
{
	delete surface; //Free up material memory
}

////////////////////////////////////SPHERE////////////////////////////////////////////////////////////////

Sphere::Sphere(Material* myMat, Point c, double r): Object(myMat), center(c), radius(r)
{
	//Nothing
}

bool Sphere::intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit)
{
	double A = (ray.rayDir.get()).dot(ray.rayDir.get());
	double B = (ray.rayDir.get() * 2.0).dot((ray.rayOrigin.get() - center.get()));
	double C = (ray.rayOrigin.get() - center.get()).dot(ray.rayOrigin.get() - center.get()) - (radius * radius);
	
	double discriminant = B*B - (4 * A * C);
	
	if (discriminant <= 0) //no hit or graze(no intersection)
	{
		return false;
	}
	else //Ray-Sphere intersection
	{
		double t1 = (-B + sqrt(discriminant)) / (2.0 * A);
		double t2 = (-B - sqrt(discriminant)) / (2.0 * A);

		if (t1 > 0) //Use t1
		{
			Vec3 hit = ray.rayOrigin.get() + ray.rayDir.get() * t1;
			pHit.set(hit);
			Vec3 normal = (hit - center.get());
			nHit.set(normal);
			tHit = t1;
		}
		else // (t2 > 0 ) //Use t2
		{
			Vec3 hit = ray.rayOrigin.get() + ray.rayDir.get() * t2;
			pHit.set(hit);
			Vec3 normal = (hit - center.get());
			nHit.set(normal);
			tHit = t2;
		}
	}

	return true;

}

////////////////////////////////////TRIANGLE//////////////////////////////////////////////////////////////
Triangle::Triangle(Material* myMat, Point a1, Point b1, Point c1): Object(myMat), a(a1), b(b1), c(c1)
{
	//Nothing
}


bool Triangle::intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit)
{
	double ma, mb, mc, md, me, mf, mg, mh, mi, mj, mk, ml;
	
	ma = a.get().x - b.get().x;
	mb = a.get().y - b.get().y;
	mc = a.get().z - b.get().z;

	md = a.get().x - c.get().x;
	me = a.get().y - c.get().y;
	mf = a.get().z - c.get().z;

	mg = ray.rayDir.get().x;
	mh = ray.rayDir.get().y;
	mi = ray.rayDir.get().z;

	mj = a.get().x - ray.rayOrigin.get().x;
	mk = a.get().y - ray.rayOrigin.get().y;
	ml = a.get().z - ray.rayOrigin.get().z;

	double ei_minus_hf = me * mi - mh * mf;
	double gf_minus_di = mg * mf - md * mi;
	double dh_minus_eg = md * mh - me * mg;

	double ak_minus_jb = ma * mk - mj * mb;
	double jc_minus_al = mj * mc - ma * ml;
	double bl_minus_kc = mb * ml - mk * mc;

	double M = ma * ei_minus_hf + mb * gf_minus_di + mc * dh_minus_eg;

	double gamma = ( mi * (ak_minus_jb) + mh * (jc_minus_al) + mg * (bl_minus_kc) ) / M;

	if (gamma < 0 || gamma > 1)
	{
		return false; //exit early, saving computations
	}

	double beta = ( mj * (ei_minus_hf) + mk * (gf_minus_di) + ml * (dh_minus_eg) ) / M;

	if (beta < 0 || beta > (1 - gamma) )
	{
		return false; //exit early, saving computations
	}

	double t = -(mf * (ak_minus_jb) + me * (jc_minus_al) + md * (bl_minus_kc) ) / M;

	//set pHit
	Vec3 hit = ray.rayOrigin.get() + ray.rayDir.get() * t;
	pHit.set(hit);
	tHit = t;

	//set nHit
	Vec3 AB = a.get() - b.get();
	Vec3 AC = a.get() - c.get();
	Vec3 N = AB.cross(AC);

	nHit.set(N);

	return true;
}


////////////////////////////////////BBOX////////////////////////////////////////////////////////////////
BBox::BBox(Material* myMat, Point n, Point f): Object(myMat), near(n), far(f)
{
	//Nothing
}


bool BBox::intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit)
{	
	enum box_side{FRONT, LEFT, RIGHT, TOP, BOTTOM};

	//t_x
	double t_near = (near.get().x - ray.rayOrigin.get().x) / ray.rayDir.get().x;
	double t_far = (far.get().x - ray.rayOrigin.get().x) / ray.rayDir.get().x;
	if (t_near > t_far) {std::swap(t_near, t_far);}

	//t_y
	double t_y_near = (near.get().y - ray.rayOrigin.get().y) / ray.rayDir.get().y;
	double t_y_far = (far.get().y - ray.rayOrigin.get().y) / ray.rayDir.get().y;

	if (t_y_near > t_y_far) {std::swap(t_y_near, t_y_far);}
    if ((t_near > t_y_far) || (t_y_near > t_far))
    {
    	return false;
    }
    if (t_y_near > t_near)
    {
    	t_near = t_y_near;	
    }    
    if (t_y_far < t_far)
    {
    	t_far = t_y_far;
    }

    //t_z
    double t_z_near = (near.get().z - ray.rayOrigin.get().z) / ray.rayDir.get().z;
    double t_z_far =  (far.get().z - ray.rayOrigin.get().z) / ray.rayDir.get().z;

    if (t_z_near > t_z_far) {std::swap(t_z_near, t_z_far);}
    if ((t_near > t_z_far) || (t_z_near > t_far))
    {
    	return false;
    }
    if (t_z_near > t_near)
    {
    	t_near = t_z_near;
    }
    if (t_z_far < t_far)
    {
    	t_far = t_z_far;
    }

    if (std::to_string(t_near) == "nan") //testing for NaN, (grazing edge of box)
    {
    	return false;
    }

    Vec3 hit(ray.rayOrigin.get() + ray.rayDir.get() * t_near);

    if (hit.y > far.get().y)
    {
    	return false;
    }

    Vec3 normal(Vec3(0,0,0));

    if (compareDoubles(hit.z, near.get().z))
    {
    	normal = Vec3(0, 0, -1);
    }
    else if (compareDoubles(hit.z, far.get().z))
    {
    	normal = Vec3(0, 0, 1);
    }
    else if (compareDoubles(hit.x, near.get().x))
    {
    	normal = Vec3(-1, 0, 0);
    }
    else if (compareDoubles(hit.x, far.get().x))
    {
    	normal = Vec3(1, 0, 0);
    }
    else if (compareDoubles(hit.y, near.get().y))
    {
    	normal = Vec3(0, -1 ,0);
    }
    else
    {
    	normal = Vec3(0, 1, 0);
    }

    pHit.set(hit);
    nHit.set(normal);
    tHit = t_near;

    return true;
     
}

bool BBox::compareDoubles(double a, double b)
{
	double eps = 0.00000001;

	return fabs(a - b) < eps;
}


////////////////////////////////////PLANE////////////////////////////////////////////////////////////////
Plane::Plane(Material* myMat, Normal n, Point p): Object(myMat), norm(n), point(p)
{
	//Nothing
}


bool Plane::intersect(Ray ray, Point& pHit, Normal& nHit, double &tHit)
{
	double t = ((point.get() - ray.rayOrigin.get()).dot(norm.get()))/(ray.rayDir.get().dot(norm.get()));
	if ( t >= 0 )
	{
		Vec3 hit(ray.rayOrigin.get() + ray.rayDir.get() * t);

		pHit.set(hit);
		nHit.set(norm.get());
		tHit = t;
		return true;
	}

	return false;
}







