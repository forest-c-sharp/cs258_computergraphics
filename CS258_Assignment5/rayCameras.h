#ifndef RAYCAMERAS_H
#define RAYCAMERAS_H

#include "rayMath.hpp"

class Camera {
public:
	Camera();

	Camera(Point o, Normal dir, Normal up, int width, int height, int AU, int AV, int BU, int BV);

	virtual ~Camera() = 0;

	virtual void setOrigin(Point newOri);
	virtual const Point& getOrigin() const;

	virtual void setDirection(Normal newDir);
	virtual const Normal& getDirection() const;

	virtual void setUp(Normal newUp);
	virtual const Normal& getUp() const;

	virtual void setWidth(int newWidth);
	virtual const int& getWidth() const;

	virtual void setHeight(int newHeight);
	virtual const int& getHeight() const;

	virtual void setDimensions(int AU, int AV, int BU, int BV);
	virtual const int& getAU() const;
	virtual const int& getAV() const;
	virtual const int& getBU() const;
	virtual const int& getBV() const;

protected:
	Point camOrigin;

	Normal camDir;

	Normal camUp;

	int screenWidth;
	int screenHeight;

	int au;
	int av;
	int bu;
	int bv;

};

class OrthoCamera: public Camera {
public:

	OrthoCamera();

	OrthoCamera(const OrthoCamera& cam);

	OrthoCamera& operator = (const OrthoCamera& rhs);

};

class PinholeCamera: public Camera {
public:

	PinholeCamera();

	PinholeCamera(const PinholeCamera& cam);

	PinholeCamera(	Point o, Normal dir, Normal up, 
					int width, int height, double s,
					int au, int av, int bu, int bv);

	PinholeCamera& operator = (const PinholeCamera p);

	void setFocalLength(double newFocal);
	const double getFocalLength() const;


private:

	double focalLength;

};


#endif