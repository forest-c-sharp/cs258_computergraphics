#include "tiffWriteModule.h"
#include <iostream>
#include <cmath>

TiffWriteModule::TiffWriteModule(GLubyte (*param)[checkImageWidth][3], TiffState* stateParam):
	checkPtr(param), tiffState(stateParam)
{
	loadTagNames();
}

std::string TiffWriteModule::invoke(char* line)
{

	if (!tiffState->fileIsValid)
	{
		return "Error: A previous tiff file has not yet been read.";
	}

	char* fileArgument = getNextArgument();
	//std::string fileName(fileArgument);
	outFile.open(fileArgument, std::ios::binary | std::ios::out);

	if (!fileArgument)
	{
		return "No filename specified";
	}

	const char* xo = getNextArgument();
	const char* yo = getNextArgument();
	const char* xc = getNextArgument();
	const char* yc = getNextArgument();

	int Xo =  (xo) ? std::atoi(xo) : 0.0;
	int Yo =  (yo) ? std::atoi(yo) : 0.0;
	int Xc =  (xc) ? std::atoi(xc) : 0.0;
	int Yc =  (yc) ? std::atoi(yc) : 0.0;

	Xo = abs(Xo);
	Yo = abs(Yo);
	Xc = abs(Xc);
	Yc = abs(Yc);

	if (Xc <= Xo || Yc <= Yo)
	{
		return "Invalid Box dimensions defined";
	}


	char littleEndian[3] = {'I', 'I', '\0'};
	outFile.write(littleEndian, sizeof(char) * 2);

	short magicNo = 42;
	outFile.write( (char*)&magicNo, sizeof(short));

	int IFDoffset = 8;
	outFile.write((char*) &IFDoffset, sizeof(int));


	writeIFD(Xo, Yo, Xc, Yc);
	
	writeImageData(Xo, Yo, Xc, Yc);


	outFile.close();
	return "Image data written to file";
}

void TiffWriteModule::writeIFD(size_t xo, size_t yo, size_t xc, size_t yc)
{
	unsigned short ifdCount(12);
	outFile.write((char*) &ifdCount, sizeof(unsigned short));


	unsigned short tag(0);
	unsigned short type(0);
	unsigned int count(0);
	unsigned int offset(0);

	bool rgb(false);
	size_t postIFDoffset(158);

	//ImageWidth 256
	tag = 256;
	type = 3;
	count = 1;
	offset = (xc-xo);

	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));


	//ImageLength 257 
	tag = 257;
	type = 3;
	count = 1;
	offset = (yc-yo);

	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));


	//Bits Per Sample 258
	tag = 258;
	type = 3;
	if (tiffState->PhotometricInterpretation == 0 || tiffState->PhotometricInterpretation == 1)
	{
		count =1;
		offset =8;

	}
	else // PI == 2 (RGB)
	{
		rgb = true;
		count = 3;
		offset = postIFDoffset;
		postIFDoffset += 6;

	}
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));

	//Compression 259
	tag = 259;
	type = 3;
	count = 1;
	offset = 1;
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));

	//Photometric Interpretation 262
	tag = 262;
	type = 3;
	count = 1;
	offset = tiffState->PhotometricInterpretation;
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));

	//Strip Offsets 273
	tag = 273;
	type = 3;
	count = 1;
	offset = 0; //Filler data

	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));

	//Save for later (to insert strip offset position)
	size_t stripOffPos = outFile.tellp();

	outFile.write((char*) &offset, sizeof(unsigned int));


	//Samples Per Pixel 277
	tag = 277;
	type = 3;
	if (!rgb)
	{
		count = 1;
		offset = 8;
	}
	else
	{
		count = 3;
		offset = postIFDoffset;
		postIFDoffset += 6;
	}
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));


	//Rows Per Strip 278
	tag = 278;
	type = 3;
	count = 1;
	offset = 2147483647;
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));


	//Strip Byte Counts 279 //Length times Width times samples
	tag = 279;
	type = 4;
	count = 1;
	if (!rgb)
	{
		offset = ( (xc-xo) * (yc-yo) ) ;
	}
	else
	{
		offset = ( (xc-xo) * (yc-yo) ) * 3;
	}
	
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));

	//XResolution 282 
	tag = 282;
	type = 5;
	count = 1;
	offset = postIFDoffset;
	postIFDoffset += 8;
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));

	//YResolution 283
	tag = 283;
	type = 5;
	count = 1;
	offset = postIFDoffset;
	postIFDoffset += 8;
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));


	//Resolution Unit 296
	tag = 296;
	type = 3;
	count = 1;
	offset = 2;
	outFile.write((char*) &tag, sizeof(unsigned short));
	outFile.write((char*) &type, sizeof(unsigned short));
	outFile.write((char*) &count, sizeof(unsigned int));
	outFile.write((char*) &offset, sizeof(unsigned int));

	// "NEXT IFD LONG 0"
	unsigned int nextIFD(0);
	outFile.write((char*) &nextIFD, sizeof(unsigned int));

	//POSTIFD DATA HERE
	if (rgb)
	{
		unsigned short bits = 8;
		unsigned short samples = 3;
		outFile.write((char*) &bits, sizeof(unsigned short));
		outFile.write((char*) &bits, sizeof(unsigned short));
		outFile.write((char*) &bits, sizeof(unsigned short));
		outFile.write((char*) &samples, sizeof(unsigned short));
		outFile.write((char*) &samples, sizeof(unsigned short));
		outFile.write((char*) &samples, sizeof(unsigned short));
	}
	//X and Y Resolution
	unsigned int numer = 72;
	unsigned int denom = 1;
	outFile.write((char*) &numer, sizeof(unsigned int));
	outFile.write((char*) &denom, sizeof(unsigned int));
	outFile.write((char*) &numer, sizeof(unsigned int));
	outFile.write((char*) &denom, sizeof(unsigned int));

	unsigned int stripDataPos = outFile.tellp();

	//Strip Offset  written here
	outFile.seekp(stripOffPos);
	outFile.write((char*) &stripDataPos, sizeof(unsigned int));
	outFile.seekp(stripDataPos);

}

void TiffWriteModule::writeImageData(size_t xo, size_t yo, size_t xc, size_t yc)
{
	bool rgb(false);
	if (tiffState->PhotometricInterpretation == 2)
	{
		rgb = true;
	}

	for (int i=yc; i > yo; --i )
	{
		for (int j = xo; j < xc; ++j)
		{
			if (!rgb)
			{
				unsigned char G = checkPtr[i][j][0];
				outFile.write((char*) &G, sizeof(unsigned char));
			}
			else
			{
				unsigned char R = checkPtr[i][j][0];
				unsigned char G = checkPtr[i][j][1];
				unsigned char B = checkPtr[i][j][2];
				outFile.write((char*) &R, sizeof(unsigned char));
				outFile.write((char*) &G, sizeof(unsigned char));
				outFile.write((char*) &B, sizeof(unsigned char));
			}
		}
	}

}


void TiffWriteModule::loadTagNames(std::string file)
{
	std::ifstream inFile;
	inFile.open(file);
	std::string currLine;

	while (!inFile.eof())
	{
		getline(inFile, currLine);
		int value = atoi(currLine.c_str());
		
		std::string code = std::to_string(value);
		std::string meaning(currLine.substr(code.size() + 1));

		tagNames.emplace(value, meaning);		
	}

	inFile.close();
}

