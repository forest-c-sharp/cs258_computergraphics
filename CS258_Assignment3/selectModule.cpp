#include "selectModule.h"
#include <string>
#include <algorithm>

SelectModule::SelectModule(ResizeModule* param): targetModule(param)
{

}

std::string SelectModule::invoke(char* line)
{
	const char* filterType = getNextArgument();

	if (!filterType)
	{
		return "No Filter Argument Specified";
	}

	std::string filterString(filterType);
	std::transform(filterString.begin(), filterString.end(),filterString.begin(), ::toupper);

	if (filterString == "LANCZOS")
	{
		targetModule->currentFilter = ResizeModule::LANCZOS;
	}
	else if (filterString == "GAUSSIAN")
	{
		targetModule->currentFilter = ResizeModule::GAUSSIAN;
	}
	else if (filterString == "HAMMING")
	{
		targetModule->currentFilter = ResizeModule::HAMMING;
	}
	else if (filterString == "MITCHELL")
	{
		targetModule->currentFilter = ResizeModule::MITCHELL;
	}
	else if (filterString == "CATMULL" || filterString == "CATMULL-ROM")
	{
		targetModule->currentFilter = ResizeModule::CATMULL;
	}
	else if (filterString == "TENT" || filterString == "TRIANGLE")
	{
		targetModule->currentFilter = ResizeModule::TENT;
	}
	else if (filterString == "BOX")
	{
		targetModule->currentFilter = ResizeModule::BOX;
	}
	else
	{
		return "Invalid String, current filter state maintained";
	}



	return "Filter Switched to " +  filterString;
}