#include "tiffStatModule.h"
#include <iostream>

TiffStatModule::TiffStatModule(): isBigEndian(false)
{
	loadTagNames();
}


std::string TiffStatModule::invoke(char* line)
{
	char* fileArgument(0);
	fileArgument = getNextArgument();
	if (!fileArgument)
	{
		return "No filename specified";
	}
	std::string fileName(fileArgument);
	inFile.open(fileArgument, std::ios::binary | std::ios::in);

	//Endianness, null-terminated char array
	//If big endian, need to byte-swap rest of file
	char buffer[3];
	inFile.read(buffer, 2);
	buffer[2] = '\0';


	if (buffer[0] != buffer[1] || ( buffer[0] != 'M' && buffer[0] != 'I' ) )
	{
		return "error: " + fileName + " is not a valid tiffFile";
	}

	if (buffer[0] == 'M')
	{
		isBigEndian = true; //if true need to byteswap
	}

	//The meaning of life (exit if not equal to 42)
	unsigned short magicNo(0); 
	inFile.read((char*)&magicNo, 2);
	swapShortBytes(&magicNo);
	if (magicNo != 42) 
	{
		isBigEndian = false;
		return "error: " + fileName + " is not a valid tiffFile";
	}

	//Process the tiff image IFD
	if (!processIFD())
	{
		isBigEndian = false;
		return "IFD error";
	}

	inFile.close();
	isBigEndian = false;
	return "Tiff File \"" + fileName + "\" stats printed to screen";

}

//Sets state of tiffState for current tiffFile
bool TiffStatModule::processIFD()
{
	unsigned int IFDptr(0);
	inFile.read((char*) &IFDptr, 4);
	swapIntBytes(&IFDptr);

	//Jump to IFD ptr
	inFile.seekg(IFDptr,std::ios::beg);

	//get IFDcount
	unsigned short IFDcount(0);
	inFile.read((char*) &IFDcount, 2);
	swapShortBytes(&IFDcount);

	//Process tags
	int previousTag(0);

	//Data For each ifd entry
	unsigned short tag;
	unsigned short type;
	unsigned int valueCount;
	unsigned int valueOffset;
	unsigned short shortOffset;
	unsigned short junk;
	//Process all of the IFD
	for (int i=0; i < IFDcount; ++i)
	{
		//Load IFD tags
		inFile.read((char*) &tag, 2);
		inFile.read((char*) &type, 2);
		inFile.read((char*) &valueCount, 4);

		//Byteswaps if necessary
		swapShortBytes(&tag);
		swapShortBytes(&type);
		swapIntBytes(&valueCount);

		if (type == 3) //Short
		{
			inFile.read((char*) &shortOffset, 2);
			inFile.read((char*) &junk, 2);
			swapShortBytes(&shortOffset);
			valueOffset = shortOffset;
		}
		else //Long data or long offset
		{
			inFile.read((char*) &valueOffset, 4);
			swapIntBytes(&valueOffset);
		}

		//Exit if tags not in order
		if (tag <= previousTag) { return false; }

		//Decode tag, print values
		auto mapIter = tagNames.find(tag);

		if (valueCount >1)
		{
			int newValueCount;
			std::string valueString("");
			size_t currPos = inFile.tellg();
			inFile.seekg(valueOffset, std::ios::beg);
			if (valueCount >10)
			{
				valueCount = 10;
			}

			for (size_t j=0; j<valueCount; ++j)
			{
				if (type == 2)
				{
				 	char value[1];
				 	inFile.read(value, 2);
				 	valueString.append(value);
				}
				else if (type == 3)
				{
					unsigned short value;
					inFile.read((char*) &value , 2);
					swapShortBytes(&value);
					valueString.append(std::to_string(value) + " ");

				}
				else if (type == 4)
				{
					unsigned int value;
					inFile.read( (char*) &value, 4);
					swapIntBytes(&value);
					valueString.append(std::to_string(value) + " ");
				}
			}
			if (valueCount >= 10)
			{
				valueString.append("...");
			}
			inFile.seekg(currPos, std::ios::beg);
			std::cout << "Tag: " << mapIter->second << "("<< mapIter->first << ")"
					<< " TagValue: " << valueCount<< "<" << valueString << ">"
						<< std::endl;
		}
		else
		{
			if (type == 5) //Rational-Process two numbers
			{
				size_t currPos = inFile.tellg();
				inFile.seekg(valueOffset, std::ios::beg);
				unsigned int numerator;
				unsigned int denominator;
				inFile.read((char*) &numerator, 4);
				inFile.read((char*) &denominator, 4);
				swapIntBytes(&numerator);
				swapIntBytes(&denominator);
				float result = numerator/denominator;
				inFile.seekg(currPos, std::ios::beg);

				std::cout << "Tag: " << mapIter->second << "("<< mapIter->first 
							<< ")"<< " TagValue: " << valueCount<< "<" << result 
							<< ">"<< std::endl;
			}
			else //Print out single value
			{
				std::cout << "Tag: " << mapIter->second << "("<< mapIter->first 
					<< ")"<< " TagValue: " << valueCount<< "<" << valueOffset 
					<< ">"<< std::endl;
			}
		}


		previousTag = tag;
	}


	return true;
}


void TiffStatModule::swapShortBytes(unsigned short * twobytes)
{
	if (isBigEndian)
	{
		unsigned char * tmp = (unsigned char *)twobytes;
		unsigned int i;
		i = tmp[1];
		tmp[1] = tmp[0];
		tmp[0] = i;
	}
}

void TiffStatModule::swapIntBytes(unsigned int * twoshorts)
{
	if (isBigEndian)
	{
		unsigned short * tmp = (unsigned short *)twoshorts;
		unsigned short i;
		i = tmp[1];
		tmp[1] = tmp[0];
		tmp[0] = i;
		swapShortBytes(&tmp[0]);
		swapShortBytes(&tmp[1]);
	}
}

void TiffStatModule::loadTagNames(std::string file)
{
	inFile.open(file);
	std::string currLine;

	while (!inFile.eof())
	{
		getline(inFile, currLine);
		int value = atoi(currLine.c_str());
		
		std::string code = std::to_string(value);
		std::string meaning(currLine.substr(code.size() + 1));

		tagNames.emplace(value, meaning);		
	}

	inFile.close();
}