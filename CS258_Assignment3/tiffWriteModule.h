#ifndef TIFFWRITEMODULE_H
#define TIFFWRITEMODULE_H

#include "module.h"
#include "preprocessors.h"
#include "tiffState.hpp"
#include <fstream>
#include <GL/glut.h>
#include <map>

class TiffWriteModule: public Module
{
public:

	TiffWriteModule(GLubyte (*param)[checkImageWidth][3], TiffState* stateParam);

	virtual std::string invoke(char* line);

private:

	void writeIFD(size_t xo, size_t yo, size_t xc, size_t yc);

	void writeImageData(size_t xo, size_t yo, size_t xc, size_t yc);

	void loadTagNames(std::string file = "Tiff_Tag_Names");

private:

	//Pointer to checkerboard image of window
	GLubyte (*checkPtr)[checkImageWidth][3];

	TiffState* tiffState;

	std::ofstream outFile;

	std::map< int, std::string> tagNames;

};

#endif