#include "readModule.h"
#include <fstream>

#include <iostream>
#include <unistd.h>

#include "dispatcher.h"


ReadModule::ReadModule(Dispatcher& disp):dispatcher(disp)
{

}

ReadModule::~ReadModule()
{

}

std::string ReadModule::invoke(char* line)
{

	char* fileArgument = getNextArgument();

	if (fileArgument)
	{
		//file name as string (for ease of returning and changing directories)
		std::string fileName(fileArgument);
		size_t finalSlash = fileName.find_last_of("/");
		std::string directory(fileName.substr(0, finalSlash));

		std::ifstream fin;
		fin.open(fileArgument);

		if (finalSlash != fileName.npos)
		{
			chdir(directory.c_str());
		}

		//Allocate Memory for a new line to process
		char* commLine = new char[1024];

		

		if (fin.good())
		{
			while (!fin.eof())
			{
				fin.getline(commLine, 1024);
				dispatcher.processCommand(commLine);
			}
			
			return "Finished Processing file " + fileName;
		}
		else
		{
			return "File \""+ fileName + "\" Does not Exist";
		}
		

		delete[] commLine;
		fin.close();

	}
	else
	{
		return "No Filename Argument Specified";
	}	

}