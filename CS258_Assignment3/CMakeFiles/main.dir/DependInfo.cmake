# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/colorModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/colorModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/dispatcher.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/dispatcher.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/drawModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/drawModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/main.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/main.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/module.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/module.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/moveModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/moveModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/readModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/readModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/resizeModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/resizeModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/selectModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/selectModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/tiffReadModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/tiffReadModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/tiffStatModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/tiffStatModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/tiffWriteModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/tiffWriteModule.cpp.o"
  "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/zoomModule.cpp" "/home/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment3/CMakeFiles/main.dir/zoomModule.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
