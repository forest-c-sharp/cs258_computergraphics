#ifndef ZOOMMODULE_H
#define ZOOMMODULE_H

#include "preprocessors.h"
#include "module.h"
#include "resizeModule.h"

class ZoomModule: public Module
{

public:

	ZoomModule(ResizeModule* param);

	virtual std::string invoke(char* line);

private:

	ResizeModule* targetModule;

};


#endif
