#ifndef SELECTMODULE_H
#define SELECTMODULE_H

#include "preprocessors.h"
#include "module.h"
#include "resizeModule.h"

class SelectModule: public Module
{

public:

	SelectModule(ResizeModule* param);

	virtual std::string invoke(char* line);

private:

	ResizeModule* targetModule;

};





#endif