#include "drawModule.h"
#include <iostream>

DrawModule::DrawModule()
{

}

DrawModule::~DrawModule()
{

}

std::string DrawModule::invoke(char* line)
{

	const char* argX = getNextArgument();
	const char* argY = getNextArgument();
	const char* argZ = getNextArgument();

	double X =  (argX) ? std::atof(argX) : 0.0;
	double Y =  (argY) ? std::atof(argY) : 0.0;
	double Z =  (argZ) ? std::atof(argZ) : 0.0;

	return ("Draw: x=" + std::to_string(X) + ", y=" + std::to_string(Y) + ", z=" + std::to_string(Z));
}