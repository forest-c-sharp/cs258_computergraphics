#ifndef TIFFSTATE_HPP
#define TIFFSTATE_HPP

#include <string>
#include <vector>

class TiffState
{
public:

	TiffState(): BitsPerSample(1), Compression(1), ImageLength(0), ImageWidth(0),
				 PhotometricInterpretation(0), ResolutionUnit(2), RowsPerStrip(32767),
				 SamplesPerPixel(1), StripByteCounts(0), StripOffsets(0),
				 XResolution(0.0), YResolution(0.0), fileIsValid(false)
	{

	}

	//258
	std::vector<unsigned int> BitsPerSample;

	//259
	unsigned int Compression;

	//257
	unsigned int ImageLength;

	//256
	unsigned int ImageWidth;

	//262
	int PhotometricInterpretation;

	//296
	unsigned int ResolutionUnit;

	//278
	unsigned int RowsPerStrip;

	//277
	unsigned int SamplesPerPixel;

	//279
	std::vector<unsigned int> StripByteCounts;

	//273
	std::vector<unsigned int> StripOffsets;

	//282
	float XResolution;

	//283
	float YResolution;

	bool fileIsValid;

};


#endif