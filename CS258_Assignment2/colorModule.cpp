#include "colorModule.h"

ColorModule::ColorModule()
{

}

ColorModule::~ColorModule()
{

}

std::string ColorModule::invoke(char* line)
{
	const char* argV = getNextArgument();
	const char* argH = getNextArgument();
	const char* argS = getNextArgument();

	double V =  (argV) ? std::atof(argV) : 0.0;
	double H =  (argH) ? std::atof(argH) : 0.0;
	double S =  (argS) ? std::atof(argS) : 0.0;

	return ("Color: V=" + std::to_string(V) + ", H=" + std::to_string(H) + ", s=" + std::to_string(S));
}