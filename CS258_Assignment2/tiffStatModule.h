#ifndef TIFFSTATMODULE_H
#define TIFFSTATMODULE_H

#include "module.h"
#include <fstream>
#include <map>
#include <string>

class TiffStatModule: public Module
{

public:

	TiffStatModule();

	//operation method 
	virtual std::string invoke(char* line);

private:

	bool processIFD();

	void swapShortBytes(unsigned short* twobytes);

	void swapIntBytes( unsigned int* twoshorts);

	void loadTagNames(std::string file = "Tiff_Tag_Names");

private:

	bool isBigEndian;

	std::ifstream inFile;

	std::map< int, std::string> tagNames;

};

#endif