cmake_minimum_required(VERSION 2.8)

SET(CMAKE_CXX_FLAGS -std=c++11)

project(main) 
add_executable(main main.cpp dispatcher.cpp module.cpp readModule.cpp drawModule.cpp moveModule.cpp colorModule.cpp tiffReadModule.cpp tiffStatModule.cpp tiffWriteModule.cpp)
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
include_directories( ${OPENGL_INCLUDE_DIRS}  ${GLUT_INCLUDE_DIRS} )

target_link_libraries(main ${OPENGL_LIBRARIES} ${GLUT_LIBRARY} )