# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/colorModule.cpp" "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/CMakeFiles/main.dir/colorModule.cpp.o"
  "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/dispatcher.cpp" "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/CMakeFiles/main.dir/dispatcher.cpp.o"
  "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/drawModule.cpp" "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/CMakeFiles/main.dir/drawModule.cpp.o"
  "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/main.cpp" "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/CMakeFiles/main.dir/main.cpp.o"
  "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/module.cpp" "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/CMakeFiles/main.dir/module.cpp.o"
  "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/moveModule.cpp" "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/CMakeFiles/main.dir/moveModule.cpp.o"
  "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/readModule.cpp" "/home/forest/Desktop/ComputerGraphics/CS258_Assignment1/CMakeFiles/main.dir/readModule.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
