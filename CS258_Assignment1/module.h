#ifndef MODULE_H
#define MODULE_H

#include <string>

/**
 *	The base class that all modules inherit from
 *
 */
class Module 
{
public:

	Module();

	//pure virtual destructor
	virtual ~Module() = 0;

	//operation method 
	virtual std::string invoke(char* line) = 0;

	//Gets next argument
	virtual char* getNextArgument();

};



#endif //MODULE_H