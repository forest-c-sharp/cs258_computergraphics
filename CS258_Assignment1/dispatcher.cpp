//FOREST C SHARP

#include "dispatcher.h"
#include <cstring>
#include <string>
#include <algorithm>
#include <utility>

#include <iostream>

#include "readModule.h"
#include "drawModule.h"
#include "moveModule.h"
#include "colorModule.h"

//Ctor:: initialize work modules
Dispatcher::Dispatcher()
{
	moduleMap.emplace("READ", new ReadModule(*this));
	moduleMap.emplace("DRAW", new DrawModule());
	moduleMap.emplace("MOVE", new MoveModule());
	moduleMap.emplace("COLOR", new ColorModule());

}

//Dtor: cleans up modules
Dispatcher::~Dispatcher() 
{ 

	for (auto it = moduleMap.begin(); it != moduleMap.end(); ++it)
	{
		delete it->second;
	}

}


void Dispatcher::processCommand(char* line)
{
	//Edge condition of comment at beginning of line
	if (line[0] == '#') return;

	//Shave off comments
	currLine = strtok(line, "#");

	//Tokenize command and convert to uppercase
   char* commToken = strtok(currLine, " ");

   //Don't attempt to construct a string out of a null char pointer(exit dispatch function early)
   if (!commToken) return;
   
   std::string commStr(commToken);
   std::transform(commStr.begin(), commStr.end(), commStr.begin(), toupper);

   //Try to find command in map
   auto mapIter = moduleMap.find(commStr);

   //if command exists, implement command, otherwise: tell user command is not supported
   if (mapIter != moduleMap.end())
   {
   	std::cout << "RESULT: " << mapIter->second->invoke(currLine) << std::endl;
   }
   else
   {
   	std::cout << "RESULT: Command Not Supported: " << currLine << std::endl;
   }

}