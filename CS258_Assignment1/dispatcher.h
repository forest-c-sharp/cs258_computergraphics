//FOREST C SHARP

#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <string>
#include <map>

#include "module.h"

/** 
 * 	Dispatcher class responsible for maintaining current input 
 * 		and delegating that input to the appropriate work modules
 */
class Dispatcher 
{

public:

	//Initializes all work modules
	Dispatcher();

	~Dispatcher();

	//Sets the current line, sends command to dispatchCommand function
	void processCommand(char* line);

private:

	char* currLine;

	//A map of key-value pairs pointing to each module
	std::map<std::string, Module*> moduleMap;

};


#endif //DISPATCHER_H