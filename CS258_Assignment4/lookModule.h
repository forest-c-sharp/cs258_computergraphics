#ifndef LOOKMODULE_H
#define LOOKMODULE_H

#include <string>
#include "transformModule.h"

class Mat4;

class LookModule: public TransformModule
{
public:

	LookModule(Mat4* ptr);

	//pure virtual destructor
	~LookModule();

	//operation method (still pure-virtual)
	virtual std::string invoke(char* line);

private:

	//Rotates the matPtr current matrix around axis vector x, y, z by degrees
	void look(	const double fx, const double fy, const double fz, 
				const double ax, const double ay, const double az, 
				const double ux, const double uy, const double uz);

};

#endif