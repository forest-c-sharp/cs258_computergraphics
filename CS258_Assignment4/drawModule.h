#ifndef DRAWMODULE_H
#define DRAWMODULE_H

#include "module.h"

class DrawModule: public Module
{
public:

	DrawModule();

	~DrawModule();

	virtual std::string invoke(char* line);
};



#endif //DRAWMODULE_H