#include "perspModule.h"
#include "mat4.hpp"
#include <cmath>
#include "src/3D.c"


PerspModule::PerspModule(Mat4* ptr, bool* perspB): TransformModule(ptr), boolPtr(perspB)
{
	persp(45.0, 1.333, 0.1, 10000.0);
}


PerspModule::~PerspModule()
{
	//Nothing
}

std::string PerspModule::invoke(char* line)
{
	const char* argFOV = getNextArgument();
	const char* argA = getNextArgument();
	const char* argN = getNextArgument();
	const char* argF = getNextArgument();

	double FOV =  (argFOV) ? std::stof(argFOV) : 45.0;
	double A =  (argA) ? std::stof(argA) : 1.33333;
	double N =  (argN) ? std::stof(argN) : 0.1;
	double F =  (argF) ? std::stof(argF) : 10000.0;

	persp(FOV,A,N,F);
	*boolPtr = true;

	return "Perspective Matrix Set";
}

void PerspModule::persp(const double fov, const double a, const double n, const double f)
{
	Near = n;
	Far = f;

	double fovy = fov;
	double coTan = 1 / tan(fovy/2);
	double a33 = (f + n)/(n-f);
	double a34 = (2 * f * n)/(n-f);

    Mat4 projMat(	coTan/a, 0, 0, 0,
        			0, coTan, 0, 0,
        			0, 0, a33,a34 ,
        			0, 0, -1 , 0 );

    *matPtr = projMat;


}











