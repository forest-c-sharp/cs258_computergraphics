#ifndef ROTATEMODULE_H
#define ROTATEMODULE_H

#include <string>
#include "transformModule.h"

class Mat4;

class RotateModule: public TransformModule
{
public:

	RotateModule(Mat4* ptr);

	//pure virtual destructor
	~RotateModule();

	//operation method (still pure-virtual)
	virtual std::string invoke(char* line);

private:

	//Rotates the matPtr current matrix around axis vector x, y, z by degrees
	void rotate(const double degrees, const double x, const double y, const double z);

};

#endif