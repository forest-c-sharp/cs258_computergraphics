#ifndef PUSHMODULE_H
#define PUSHMODULE_H

#include "transformModule.h"
#include <string>
#include <stack>
#include "mat4.hpp"

/**
 *	The base class that all modules inherit from
 *
 */
class PushModule: public TransformModule 
{
public:

	PushModule(Mat4* matPtr, std::stack<Mat4>* stackP);

	//pure virtual destructor
	virtual ~PushModule();

	//operation method 
	virtual std::string invoke(char* line);

private:

	std::stack<Mat4>* stackPtr;

};



#endif //PUSHMODULE_H