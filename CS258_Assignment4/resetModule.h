#ifndef RESETMODULE_H
#define RESETMODULE_H

#include "transformModule.h"
#include <string>
#include <stack>
#include "mat4.hpp"

/**
 *	The base class that all modules inherit from
 *
 */
class ResetModule: public TransformModule 
{
public:

	ResetModule(Mat4* matPtr, std::stack<Mat4>* stackP);

	//pure virtual destructor
	virtual ~ResetModule();

	//operation method 
	virtual std::string invoke(char* line);

private:

	std::stack<Mat4>* stackPtr;

};



#endif //RESETMODULE_H