#include <string>
#include "transformModule.h"

class Mat4;

class TranslateModule: public TransformModule
{
public:

	TranslateModule(Mat4* ptr);

	//pure virtual destructor
	~TranslateModule();

	//operation method (still pure-virtual)
	virtual std::string invoke(char* line);

private:

	//Translates by x, y, and z
	void translate(const double x, const double y, const double z);

};