#include "resetModule.h"
#include <stack>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

ResetModule::ResetModule(Mat4* matPtr, std::stack<Mat4>* stackP): TransformModule(matPtr), stackPtr(stackP)
{

}

ResetModule::~ResetModule()
{
	//Nothing
}

std::string ResetModule::invoke(char* line)
{
	while (stackPtr->size() > 1)
	{
		stackPtr->pop();
	}

	*matPtr = Mat4();

	glClear(GL_COLOR_BUFFER_BIT);
	glRasterPos2i(0,0);
	glFlush();

	return "Screen Cleared, Stack Cleared, Matrix C restored to Identity.";
}