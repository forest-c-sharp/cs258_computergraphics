#include "popModule.h"
#include <stack>

PopModule::PopModule(Mat4* matPtr, std::stack<Mat4>* stackP): TransformModule(matPtr), stackPtr(stackP)
{

}

PopModule::~PopModule()
{
	//Nothing
}

std::string PopModule::invoke(char* line)
{

	if (stackPtr->size() == 1)
	{
		return "Stack is at its minimum size.";
	}

	*matPtr = stackPtr->top();
	stackPtr->pop();
	return "Matrix Popped off Stack";
}