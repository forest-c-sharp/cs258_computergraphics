//FOREST C SHARP

#include "dispatcher.h"
#include <cstring>
#include <string>
#include <algorithm>
#include <utility>

#include <iostream>

#include "readModule.h"
#include "drawModule.h"
#include "moveModule.h"
#include "colorModule.h"
#include "tiffReadModule.h"
#include "tiffStatModule.h"
#include "tiffWriteModule.h"
#include "resizeModule.h"
#include "zoomModule.h"
#include "selectModule.h"
#include "rotateModule.h"
#include "scaleModule.h"
#include "translateModule.h"
#include "orthoModule.h"
#include "perspModule.h"
#include "pushModule.h"
#include "popModule.h"
#include "resetModule.h"
#include "lookModule.h"
#include "vertexModule.h"

//Ctor:: initialize work modules
Dispatcher::Dispatcher(GLubyte (*param)[checkImageWidth][3]): checkPtr(param), currLine(0),
                                                      moduleMap(), tiffState(), currentMatrix(),
                                                      perspMatrix(), orthoMatrix(),
                                                      usePerspective(false), matrixStack()
{
   //Assn 1
	moduleMap.emplace("READ", new ReadModule(*this));
	moduleMap.emplace("DRAW", new DrawModule());
	moduleMap.emplace("MOVE", new MoveModule());
	moduleMap.emplace("COLOR", new ColorModule());

   //Assn 2
   moduleMap.emplace("TIFFREAD", new TiffReadModule(checkPtr, &tiffState));
   moduleMap.emplace("TIFFSTAT" , new TiffStatModule());
   moduleMap.emplace("TIFFWRITE", new TiffWriteModule(checkPtr, &tiffState));

   //Assn3
   ResizeModule* reMod = new ResizeModule(checkPtr, &tiffState);
   moduleMap.emplace("RESIZE", reMod);
   moduleMap.emplace("ZOOM", new ZoomModule(reMod));
   moduleMap.emplace("SELECT", new SelectModule(reMod));

   //Assn4
   moduleMap.emplace("ROTATE", new RotateModule(&currentMatrix));
   moduleMap.emplace("SCALE", new ScaleModule(&currentMatrix));
   moduleMap.emplace("TRANSLATE", new TranslateModule(&currentMatrix));
   moduleMap.emplace("LOOKAT", new LookModule(&currentMatrix));
   
   moduleMap.emplace("ORTHO", new OrthoModule(&orthoMatrix, &usePerspective));
   PerspModule* pMod = new PerspModule(&perspMatrix, &usePerspective);
   moduleMap.emplace("PERSPECTIVE", pMod);

   Mat4 identity; //Could have used one of the already initialized matrices. THis is for readability
   matrixStack.push(identity); //Matrix will always have the identity at its base

   moduleMap.emplace("PUSH", new PushModule(&currentMatrix, &matrixStack));
   moduleMap.emplace("POP", new PopModule(&currentMatrix, &matrixStack));
   moduleMap.emplace("RESET", new ResetModule(&currentMatrix, &matrixStack));

   moduleMap.emplace("VERTEX", new VertexModule(&perspMatrix, &orthoMatrix, &currentMatrix, &usePerspective, pMod));


}

//Dtor: cleans up modules
Dispatcher::~Dispatcher() 
{ 

	for (auto it = moduleMap.begin(); it != moduleMap.end(); ++it)
	{
		delete it->second;
	}

}


void Dispatcher::processCommand(char* line)
{
	//Edge condition of comment at beginning of line
	if (line[0] == '#') return;

	//Shave off comments
	currLine = strtok(line, "#");

	//Tokenize command and convert to uppercase
   char* commToken = strtok(currLine, " ");

   //Don't attempt to construct a string out of a null char pointer(exit dispatch function early)
   if (!commToken) return;
   
   std::string commStr(commToken);
   std::transform(commStr.begin(), commStr.end(), commStr.begin(), toupper);

   //Try to find command in map
   auto mapIter = moduleMap.find(commStr);

   //if command exists, implement command, otherwise: tell user command is not supported
   if (mapIter != moduleMap.end())
   {
   	std::cout << "RESULT: " << mapIter->second->invoke(currLine) << std::endl;
   }
   else
   {
   	std::cout << "RESULT: Command Not Supported: " << currLine << std::endl;
   }

}