#ifndef PERSPMODULE_H
#define PERSPMODULE_H

#include <string>
#include "transformModule.h"

class Mat4;

class PerspModule: public TransformModule
{
public:

	PerspModule(Mat4* ptr, bool* persp);

	//pure virtual destructor
	~PerspModule();

	//operation method (still pure-virtual)
	virtual std::string invoke(char* line);

private:

	//Rotates the matPtr current matrix around axis vector x, y, z by degrees
	void persp(const double fov, const double a, const double n, const double f);

	bool* boolPtr;

};

#endif