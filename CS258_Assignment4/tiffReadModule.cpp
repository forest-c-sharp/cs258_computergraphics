#include "tiffReadModule.h"

#include <fstream>
#include <iostream>
#include <cstring>


TiffReadModule::TiffReadModule(GLubyte (*param)[checkImageWidth][3], TiffState* stateParam):
	isBigEndian(false), checkPtr(param), tiffState(stateParam)
{
	loadTagNames();
}

std::string TiffReadModule::invoke(char* line)
{
	char* fileArgument(0);
	fileArgument = getNextArgument();
	if (!fileArgument)
	{
		return "No filename specified";
	}
	std::string fileName(fileArgument);
	inFile.open(fileArgument, std::ios::binary | std::ios::in);

	//Endianness, null-terminated char array
	//If big endian, need to byte-swap rest of file
	char buffer[3];
	inFile.read(buffer, 2);
	buffer[2] = '\0';

	if (buffer[0] != buffer[1] || ( buffer[0] != 'M' && buffer[0] != 'I' ) )
	{
		return "error: " + fileName + " is not a valid tiffFile";
	}

	if (buffer[0] == 'M')
	{
		isBigEndian = true; //if true need to byteswap
	}

	//The meaning of life (exit if not equal to 42)
	unsigned short magicNo(0); 
	inFile.read((char*)&magicNo, 2);
	swapShortBytes(&magicNo);
	if (magicNo != 42) 
	{
		isBigEndian = false;
		return "error: " + fileName + " is not a valid tiffFile";
	}

	//Process the tiff image IFD
	if (!processIFD())
	{
		isBigEndian = false;
		return "IFD error";
	}
	//Process the tiff image data
	if (!testForReadability())
	{
		return "Tiff file not supported by this reader";
	}

	processImageData();
	inFile.close();
	isBigEndian = false;
	tiffState->fileIsValid = true;
	return "Tiff File \"" + fileName + "\" loaded to screen";

}

//Sets state of tiffState for current tiffFile
bool TiffReadModule::processIFD()
{
	unsigned int IFDptr(0);
	inFile.read((char*) &IFDptr, 4);
	swapIntBytes(&IFDptr);

	//Jump to IFD ptr
	inFile.seekg(IFDptr,std::ios::beg);

	//get IFDcount
	unsigned short IFDcount(0);
	inFile.read((char*) &IFDcount, 2);
	swapShortBytes(&IFDcount);

	//Process tags
	int previousTag(0);

	//Data For each ifd entry
	unsigned short tag;
	unsigned short type;
	unsigned int valueCount;
	unsigned int valueOffset;
	unsigned short shortOffset;
	unsigned short junk;
	//Process all of the IFD
	for (int i=0; i < IFDcount; ++i)
	{
		//Load IFD tags
		inFile.read((char*) &tag, 2);
		inFile.read((char*) &type, 2);
		inFile.read((char*) &valueCount, 4);

		//Byteswaps if necessary
		swapShortBytes(&tag);
		swapShortBytes(&type);
		swapIntBytes(&valueCount);

		if (type == 3) //Short
		{
			inFile.read((char*) &shortOffset, 2);
			inFile.read((char*) &junk, 2);
			swapShortBytes(&shortOffset);
			valueOffset = shortOffset;
		}
		else //Long data or long offset
		{
			inFile.read((char*) &valueOffset, 4);
			swapIntBytes(&valueOffset);
		}

		//Exit if tags not in order
		if (tag <= previousTag) { return false; }

		//Decode tag, updating tiffState if necessary
		auto mapIter = tagNames.find(tag);
		processTag(tag, type, valueCount, valueOffset);



		previousTag = tag;
	}


	return true;
}

//Processes tag and updates tiffState
void TiffReadModule::processTag(unsigned short tag, unsigned short type, unsigned int count, unsigned int offset)
{
	size_t currPos(0);

	switch(tag)
	{
		//ImageWidth
		case 256:
			tiffState->ImageWidth = offset;
			break;

		//ImageLength
		case 257:
			tiffState->ImageLength = offset;
			break;

		//Bits Per Sample
		case 258:
			//Get Count, parse offset appropriately
			tiffState->BitsPerSample.resize(count);

			for (size_t i=0; i < tiffState->BitsPerSample.size(); ++i)
			{
				tiffState->BitsPerSample[i] = 8;
			}

			break;

		//Compression
		case 259:
			tiffState->Compression = offset;
			break;

		//Photometric Interpretation
		case 262:
			tiffState->PhotometricInterpretation = offset;
			break;

		//Strip Offsets
		case 273:
			//Resize strip offsets
			tiffState->StripOffsets.resize(count);
			if (count == 1)
			{
				tiffState->StripOffsets[0] = offset;
			}
			else
			{
				//Jump to offset and process each (size of count), then jump back
				currPos = inFile.tellg();
				inFile.seekg(offset, std::ios::beg);
				for (size_t i = 0; i < count; ++i)
				{
					if (type == 3)
					{
						unsigned short stripPtr;
						inFile.read((char*) &stripPtr , 2);
						swapShortBytes(&stripPtr);
						tiffState->StripOffsets[i] = stripPtr;
					}
					else
					{
						unsigned int stripPtr;
						inFile.read( (char*) &stripPtr, 4);
						swapIntBytes(&stripPtr);
						tiffState->StripOffsets[i] = stripPtr;
					}
				}
				inFile.seekg(currPos, std::ios::beg);
			}
			break;

		//Samples Per Pixel
		case 277:
			tiffState->SamplesPerPixel = offset;
			break;

		//Rows Per Strip
		case 278:
			tiffState->RowsPerStrip = offset;
			break;

		//Strip Byte Counts
		case 279:
			//Resize strip byte counts
			tiffState->StripByteCounts.resize(count);
			if (count == 1)
			{
				tiffState->StripByteCounts[0] = offset;
			}
			else
			{
				//Jump to offset and process each (size of count), then jump back
				currPos = inFile.tellg();
				inFile.seekg(offset, std::ios::beg);
				for (int i = 0; i < count; ++i)
				{
					if (type == 3)
					{
						unsigned short stripPtr;
						inFile.read((char*) &stripPtr , 2);
						swapShortBytes(&stripPtr);
						tiffState->StripByteCounts[i] = stripPtr;
					}
					else
					{
						unsigned int stripPtr;
						inFile.read((char*) &stripPtr , 4);
						swapIntBytes(&stripPtr);
						tiffState->StripByteCounts[i] = stripPtr;
					}
				}
				inFile.seekg(currPos);
			}
			break;

		//X Resolution (RATIONAL)
		case 282:
			
			break;

		//Y Resolution (RATIONAL)
		case 283:

			break;

		//Resolution Unit
		case 296:
			tiffState->ResolutionUnit = offset;
			break;

		//If not a case we care about for tiffState, break
		default:
			break;
	}

}

//Processes current tiffImage based on TiffState and displays
void TiffReadModule::processImageData()
{
	size_t yCoord(tiffState->ImageLength);

	for (int i = 0; i < tiffState->StripOffsets.size(); ++i)
	{ 
		//Seek to current strip
		inFile.seekg(tiffState->StripOffsets[i], std::ios::beg);

		for (int j = 0; j < tiffState->StripByteCounts[i]; ++j)
		{	
			size_t xCoord = j % tiffState->ImageWidth;
			if (xCoord == 0)
			{
				--yCoord;
			}

			//Process each byte
			if (tiffState->PhotometricInterpretation == 0 || tiffState->PhotometricInterpretation == 1)
			{
				unsigned char G, junk;
				inFile.read((char*) &G, 1);

				//Reverse blacks and whites if 0
				if (tiffState->PhotometricInterpretation == 0 )
				{
					G = 255 -G;
				}

				//Shave off extra samples
				for (int i= 1; i < tiffState->BitsPerSample.size(); ++i)
				{
					inFile.read((char*) &junk, 1);
				}
				checkPtr[yCoord][xCoord][0] = G;
				checkPtr[yCoord][xCoord][1] = G;
				checkPtr[yCoord][xCoord][2] = G;

			}
			else if (tiffState->PhotometricInterpretation == 2)
			{	
				unsigned char R, G, B, junk;
				inFile.read((char*) &R, 1);
				inFile.read((char*) &G, 1);
				inFile.read((char*) &B, 1);


				//Shave off extra samples
				for (int i= 3; i < tiffState->BitsPerSample.size(); ++i)
				{
					inFile.read((char*) &junk, 1);
				}

				checkPtr[yCoord][xCoord][0] = R;
				checkPtr[yCoord][xCoord][1] = G;
				checkPtr[yCoord][xCoord][2] = B;

			}

			if (yCoord == 0) return;
		}
	}


}

//Tests the current tiffState for readability
bool TiffReadModule::testForReadability()
{
	bool readable(true);

	for (size_t i=0; i < tiffState->BitsPerSample.size(); ++i)
	{
		if (tiffState->BitsPerSample[i] != 8)
		{
			std:: cout << tiffState->BitsPerSample[i] << std::endl;
			readable = false;
		}
	}

	if (tiffState->PhotometricInterpretation != 0 
		&& tiffState->PhotometricInterpretation != 1 
		&& tiffState->PhotometricInterpretation != 2)
	{
		readable = false;
	}


	return readable;
}


void TiffReadModule::swapShortBytes(unsigned short * twobytes)
{
	if (isBigEndian)
	{
		unsigned char * tmp = (unsigned char *)twobytes;
		unsigned int i;
		i = tmp[1];
		tmp[1] = tmp[0];
		tmp[0] = i;
	}
}

void TiffReadModule::swapIntBytes(unsigned int * twoshorts)
{
	if (isBigEndian)
	{
		unsigned short * tmp = (unsigned short *)twoshorts;
		unsigned short i;
		i = tmp[1];
		tmp[1] = tmp[0];
		tmp[0] = i;
		swapShortBytes(&tmp[0]);
		swapShortBytes(&tmp[1]);
	}
}

void TiffReadModule::loadTagNames(std::string file)
{
	inFile.open(file);
	std::string currLine;

	while (!inFile.eof())
	{
		getline(inFile, currLine);
		int value = atoi(currLine.c_str());
		
		std::string code = std::to_string(value);
		std::string meaning(currLine.substr(code.size() + 1));

		tagNames.emplace(value, meaning);		
	}

	inFile.close();
}
