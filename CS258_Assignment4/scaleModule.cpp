#include "scaleModule.h"
#include "mat4.hpp"
#include <cmath>


ScaleModule::ScaleModule(Mat4* ptr): TransformModule(ptr)
{

}

ScaleModule::~ScaleModule()
{
	//Nothing
}

std::string ScaleModule::invoke(char* line)
{

	const char* argX = getNextArgument();
	const char* argY = getNextArgument();
	const char* argZ = getNextArgument();

	double X =  (argX) ? std::stof(argX) : 0.0;
	double Y =  (argY) ? std::stof(argY) : 0.0;
	double Z =  (argZ) ? std::stof(argZ) : 0.0;

	scale(X, Y, Z);

	return "Matrix C Scaled by values x=" + std::to_string(X) + ", y=" + std::to_string(Y) + ", z=" + std::to_string(Z);
}

void ScaleModule::scale(const double x, const double y, const double z)
{

	Mat4 scalingMatrix(	x, 0., 0., 0.,
						0., y, 0., 0.,
						0., 0., z, 0.,
						0., 0., 0., 1.);

	*matPtr = *matPtr * scalingMatrix;
	
}