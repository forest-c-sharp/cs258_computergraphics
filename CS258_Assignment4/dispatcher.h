//FOREST C SHARP

#ifndef DISPATCHER_H
#define DISPATCHER_H

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <string>
#include <map>
#include <stack>

#include "module.h"
#include "preprocessors.h"
#include "tiffState.hpp"
#include "mat4.hpp"

/** 
 * 	Dispatcher class responsible for maintaining current input 
 * 		and delegating that input to the appropriate work modules
 */
class Dispatcher 
{

public:

	//Initializes all work modules
	Dispatcher(GLubyte (*param)[checkImageWidth][3]);

	~Dispatcher();

	//Sets the current line, sends command to dispatchCommand function
	void processCommand(char* line);

private:

	//Pointer to checkerboard image of window
	GLubyte (*checkPtr)[checkImageWidth][3];

	char* currLine;

	//A map of key-value pairs pointing to each module
	std::map<std::string, Module*> moduleMap;

	//State of lastTiff read
	TiffState tiffState;

	//Global Primary Matrix C
	Mat4 currentMatrix;

	//Perspective Transformation matrix
	Mat4 perspMatrix;

	//Orthographic Transformation matrix
	Mat4 orthoMatrix;

	//If true, use persective, if false use ortho
	bool usePerspective;

	//Matrix Stack
	std::stack<Mat4> matrixStack;

};


#endif //DISPATCHER_H