#include "transformModule.h"
#include <cmath>

TransformModule::TransformModule(Mat4* ptr): matPtr(ptr)
{
	//empty
}

TransformModule::~TransformModule()
{
	//empty
}

double TransformModule::toRadians(double degrees)
{
	return (degrees * (M_PI/ 180.));
}