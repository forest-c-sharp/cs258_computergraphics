#include <string>
#include "transformModule.h"

class Mat4;

class ScaleModule: public TransformModule
{
public:

	ScaleModule(Mat4* ptr);

	//pure virtual destructor
	~ScaleModule();

	//operation method (still pure-virtual)
	virtual std::string invoke(char* line);

private:

	//Rotates the matPtr current matrix around axis vector x, y, z by degrees
	void scale(const double x, const double y, const double z);

};