# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/colorModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/colorModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/dispatcher.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/dispatcher.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/drawModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/drawModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/lookModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/lookModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/main.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/main.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/module.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/module.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/moveModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/moveModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/orthoModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/orthoModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/perspModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/perspModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/popModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/popModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/pushModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/pushModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/readModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/readModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/resetModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/resetModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/resizeModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/resizeModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/rotateModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/rotateModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/scaleModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/scaleModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/selectModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/selectModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/tiffReadModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/tiffReadModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/tiffStatModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/tiffStatModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/tiffWriteModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/tiffWriteModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/transformModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/transformModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/translateModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/translateModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/vertexModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/vertexModule.cpp.o"
  "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/zoomModule.cpp" "/Users/forestsharp/Desktop/cs258_computergraphics/CS258_Assignment4/CMakeFiles/main.dir/zoomModule.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
