#ifndef VERTEXMODULE_H
#define VERTEXMODULE_H

#include "module.h"
#include "mat4.hpp"
#include "perspModule.h"

class VertexModule: public Module
{
public:

	VertexModule(Mat4* p, Mat4* o, Mat4* c, bool* b, PerspModule* ptr);

	~VertexModule();

	std::string invoke(char* line);

private:

	Mat4* perspectiveMatrix;

	Mat4* orthographicMatrix;

	Mat4* currentMatrix;

	bool* usePerspective;

	PerspModule* perspPtr;

};


#endif //VERTEXMODULE_H