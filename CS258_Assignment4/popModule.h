#ifndef POPMODULE_H
#define POPMODULE_H

#include "transformModule.h"
#include <string>
#include <stack>
#include "mat4.hpp"

/**
 *	The base class that all modules inherit from
 *
 */
class PopModule: public TransformModule 
{
public:

	PopModule(Mat4* matPtr, std::stack<Mat4>* stackP);

	//pure virtual destructor
	virtual ~PopModule();

	//operation method 
	virtual std::string invoke(char* line);

private:

	std::stack<Mat4>* stackPtr;

};



#endif //POPMODULE_Hm