#include <string>
#include "transformModule.h"

class Mat4;

class OrthoModule: public TransformModule
{
public:

	OrthoModule(Mat4* ptr, bool* persp);

	//pure virtual destructor
	~OrthoModule();

	//operation method (still pure-virtual)
	virtual std::string invoke(char* line);

private:

	//Rotates the matPtr current matrix around axis vector x, y, z by degrees
	void ortho(const double l, const double r, const double b, const double t, const double n, const double f);

	bool* boolPtr;


};