#include "lookModule.h"
#include "mat4.hpp"
#include <cmath>

#include "src/3D.c"


LookModule::LookModule(Mat4* ptr): TransformModule(ptr)
{

}

LookModule::~LookModule()
{
	//Nothing
}

std::string LookModule::invoke(char* line)
{
	const char* argFX = getNextArgument();
	const char* argFY = getNextArgument();
	const char* argFZ = getNextArgument();
	const char* argAX = getNextArgument();
	const char* argAY = getNextArgument();
	const char* argAZ = getNextArgument();
	const char* argUX = getNextArgument();
	const char* argUY = getNextArgument();
	const char* argUZ = getNextArgument();


	double FX = 	(argFX) ? std::stof(argFX) : 1.0;
	double FY =  	(argFY) ? std::stof(argFY) : 1.0;
	double FZ =  	(argFZ) ? std::stof(argFZ) : 1.0;
	double AX = 	(argAX) ? std::stof(argAX) : 0.0;
	double AY =  	(argAY) ? std::stof(argAY) : 0.0;
	double AZ =  	(argAZ) ? std::stof(argAZ) : 0.0;
	double UX = 	(argUX) ? std::stof(argUX) : 0.0;
	double UY =  	(argUY) ? std::stof(argUY) : 1.0;
	double UZ =  	(argUZ) ? std::stof(argUZ) : 0.0;


	look(FX,FY,FZ, AX, AY, AZ, UX, UY, UZ);

	return "Looked at new point from new position";
}

void LookModule::look(	const double fx, const double fy, const double fz, 
						const double ax, const double ay, const double az, 
						const double ux, const double uy, const double uz)
{

	for (int i=0; i < 4; ++i)
	{
		for (int j=0; j < 4; ++j)
		{
			current.mat[i][j]	=	(*matPtr)[i][j];
		}
	}

	gtLookAt(fx, fy, fz, ax, ay, az, ux, uy, uz);

	for (int i=0; i < 4; ++i)
	{
		for (int j=0; j < 4; ++j)
		{
			(*matPtr)[i][j]	= current.mat[i][j];
		}
	}

}








