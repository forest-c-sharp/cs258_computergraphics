#ifndef MAT4_HPP
#define MAT4_HPP

#include <iostream>
#include <cmath>

class Mat4 
{

public:

	//Def ctor: identity matrix
	Mat4()
	{
		for (int i=0; i<4; ++i)
		{
			for (int j=0; j<4; ++j)
			{
				if (i ==j)
				{
					matrixData[i][j] = 1.0;
				}
				else
				{
					matrixData[i][j] = 0.0;
				}
			}
		}
	}

	//Copy Ctor
	Mat4(const Mat4 &rhs)
	{
		for (int i=0; i<4; ++i) //columns
		{
			for (int j=0; j<4; ++j) //rows
			{
				matrixData[i][j] = rhs.matrixData[i][j];
			}
		}
	}

	//Full broken-out constructor
	Mat4(	double x11, double x12, double x13, double x14,
			double x21, double x22, double x23, double x24,
			double x31, double x32, double x33, double x34,
			double x41, double x42, double x43, double x44):
	matrixData{	{x11, x12, x13, x14},
				{x21, x22, x23, x24},
				{x31, x32, x33, x34},
				{x41, x42, x43, x44}}
	{
		//nothing left to do
	}

	//Assignment operator
	Mat4& operator=(const Mat4& rhs)
	{
		if (this != &rhs)
		{
			for (int i=0; i < 4; ++i) {
				for (int j=0; j<4; ++j) {
					matrixData[i][j] = rhs.matrixData[i][j];
				}
			}
		}
		
		return *this;
	}

	//Matrix multiplication
	Mat4 operator*(const Mat4& rhs)
	{
		Mat4 newMat;

		for (int i=0; i<4; ++i) //Column of new mat
		{
			for (int j=0; j<4; ++j) //Row of new mat
			{
				newMat.matrixData[i][j] = 	matrixData[0][j] * rhs.matrixData[i][0] +
											matrixData[1][j] * rhs.matrixData[i][1] +
											matrixData[2][j] * rhs.matrixData[i][2] +
											matrixData[3][j] * rhs.matrixData[i][3];
			}
		}

		return newMat;
	}

	Mat4& operator*=(const Mat4& rhs)
	{
		*this = *this * rhs;
		return *this;
	}

	//Matrix addition
	Mat4 operator+(const Mat4& rhs)
	{
		Mat4 newMat;
		for (int i=0; i<4; ++i)
		{
			for (int j=0; j<4; ++j)
			{
				newMat.matrixData[i][j] = matrixData[i][j] + rhs.matrixData[i][j];
			}
		}
		return newMat;
	}

	Mat4& operator+=(const Mat4& rhs)
	{
		*this = *this + rhs;
		return *this;
	}

	double* operator[](size_t i)
	{
		return matrixData[i];
	}

private:

	//Data for matrix: [columns][rows]
	double matrixData[4][4];


};

inline std::ostream& operator<<(std::ostream& os, Mat4& mat)
{
	for (int i=0; i<4; ++i) 
	{
		for (int j=0; j<4; ++j) 
		{
			os << mat[i][j] << " ";
		}
		os << std::endl;
	}
	return os;
}

class Vec3
{
public:
	Vec3()
	{
		for (int i=0; i<3; ++i)
		{
			data[i] = 0.0;
		}
	}

	//Copy Ctor
	Vec3(const Vec3 &rhs)
	{
		for (int i=0; i<3; ++i) //columns
		{
			data[i] = rhs.data[i];
		}
	}

	Vec3(double x, double y, double z)
	{
		data[0] = x;
		data[1] = y;
		data[2] = z;
	}

	//Dot product
	double operator*(const Vec3 &rhs)
	{
		return data[0] * rhs.data[0] + data[1] * rhs.data[1] + data[2] * rhs.data[2];
	}

	Vec3 operator-(const Vec3 &rhs)
	{
		Vec3 result;
		result.data[0] = data[0] - rhs.data[0];
		result.data[1] = data[1] - rhs.data[1];
		result.data[2] = data[2] - rhs.data[2];
		return result;
	}

	Vec3 cross(const Vec3 & rhs)
	{
		Vec3 result;

		result.data[0] = data[1] * rhs.data[2] - data[2] * rhs.data[1];
		result.data[1] = data[2] * rhs.data[0] - data[0] * rhs.data[2];
		result.data[2] = data[0] * rhs.data[1] - data[1] * rhs.data[0];

		return result;
	}

	Vec3 normalize()
	{
		Vec3 result;
		double length = sqrt(data[0] * data[0] + data[1] * data[1] + data[2] *data[2]);
		result.data[0] = data[0] / length;
		result.data[1] = data[1] / length;
		result.data[2] = data[2] / length;

		return result;
	}

public:
	
	double data[3];

};










#endif