#ifndef TRANSFORMMODULE_H
#define TRANSFORMMODULE_H

#include <string>
#include "module.h"

class Mat4; //Forward declaration

class TransformModule: public Module
{
public:

	TransformModule(Mat4* ptr);

	//pure virtual destructor
	virtual ~TransformModule() = 0;

	//operation method (still pure-virtual)
	virtual std::string invoke(char* line) = 0;

protected:

	double toRadians(double degrees);

	//Pointer to the current matrix to apply a given transform to
	Mat4* matPtr;

};







#endif