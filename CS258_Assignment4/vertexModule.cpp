#include "vertexModule.h"
#include "src/3D.c"

VertexModule::VertexModule(Mat4* p, Mat4* o, Mat4* c, bool* b, PerspModule* ptr): perspectiveMatrix(p), 
									orthographicMatrix(p), currentMatrix(c), usePerspective(b), perspPtr(ptr)

{

}

VertexModule::~VertexModule()
{

}

std::string VertexModule::invoke(char* line)
{
	const char* argX = getNextArgument();
	const char* argY = getNextArgument();
	const char* argZ = getNextArgument();

	double X =  (argX) ? std::stof(argX) : 0.0;
	double Y =  (argY) ? std::stof(argY) : 0.0;
	double Z =  (argZ) ? std::stof(argZ) : 0.0;


	perspflag = *usePerspective;
	std::cout << std::endl;
	for (int i=0; i < 4; ++i)
	{
		for (int j=0; j < 4; ++j)
		{
			perspect.mat[i][j]	=	(*perspectiveMatrix)[i][j];
			std::cout << perspect.mat[i][j] << " ";
			orth.mat[i][j] 		= 	(*orthographicMatrix)[i][j];
			current.mat[i][j]	=	(*currentMatrix)[i][j];
		}
		std::cout << std::endl;
	}

	glRasterPos2i(0,0);
	gtVertex3f(X, Y, Z);

	return ("Vertex: x=" + std::to_string(X) + ", y=" + std::to_string(Y) + ", z=" + std::to_string(Z));
}