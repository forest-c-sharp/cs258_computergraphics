#ifndef MOVEMODULE_H
#define MOVEMODULE_H

#include "module.h"

class MoveModule: public Module
{
public:

	MoveModule();

	~MoveModule();

	std::string invoke(char* line);


};


#endif //MOVEMODULE_H