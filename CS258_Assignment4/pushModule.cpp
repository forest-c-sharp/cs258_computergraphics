#include "pushModule.h"
#include <stack>

PushModule::PushModule(Mat4* matPtr, std::stack<Mat4>* stackP): TransformModule(matPtr), stackPtr(stackP)
{

}

PushModule::~PushModule()
{
	//Nothing
}

std::string PushModule::invoke(char* line)
{

	stackPtr->push(*matPtr);

	return "Matrix C Pushed Onto Stack";
}