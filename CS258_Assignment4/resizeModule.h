#ifndef RESIZEMODULE_H
#define RESIZEMODULE_H

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "preprocessors.h"
#include "module.h"
#include "tiffState.hpp"

class ResizeModule: public Module
{

public:

	ResizeModule(GLubyte (*param)[checkImageWidth][3], TiffState* stateParam);

	virtual std::string invoke(char* line);

	enum filterType {LANCZOS, GAUSSIAN, MITCHELL, CATMULL, HAMMING, TENT, BOX};

	filterType currentFilter;

private:

	//One Dimensional Scale-up Function
	virtual void scaleUp(GLubyte (*source)[3], GLubyte (*target)[3], double N, double K);

	//One Dimensional Scale-down Function
	virtual void scaleDown(GLubyte (*source)[3], GLubyte (*target)[3], double N, double K);

	//Scale up convolution
	virtual double reconstructAtPointUp(double x, GLubyte (*source)[3], double N, size_t index);

	//Scale down convolution
	virtual double reconstructAtPointDown(double x, GLubyte (*source)[3], double N, double K, size_t index);

	//General filter function: selects appropriate filter based on enum
	virtual double filterSelect(double t);

	//Default filter kernel
	virtual double lanczosFilter(double t, double a);

	//Gaussian filter kernel
	virtual double gaussianFilter(double t);

	//Mitchell filter kernel
	virtual double mitchellFilter(double t, double B, double C);

	//Catmull-rom filter
	virtual double catmullRomFilter(double t);

	//Hamming window
	virtual double hammingFilter(double t);

	//Tent filter
	virtual double tentFilter(double t);

	//Box filter
	virtual double boxFilter(double t);

	//Sinc Function
	virtual double sinc(double x);

	//Clamps a double to range 0 - 255
	virtual GLubyte clamp(double arg);

	//Fill in black if necessary
	virtual void border(int oldWidth, int oldHeight, int newWidth, int newHeight);

	//Pointer to checkerboard image of window
	GLubyte (*checkPtr)[checkImageWidth][3];

	TiffState* tiffState;

};



#endif 