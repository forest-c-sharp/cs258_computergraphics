#include "translateModule.h"
#include "mat4.hpp"
#include <cmath>


TranslateModule::TranslateModule(Mat4* ptr): TransformModule(ptr)
{

}

TranslateModule::~TranslateModule()
{
	//Nothing
}

std::string TranslateModule::invoke(char* line)
{

	const char* argX = getNextArgument();
	const char* argY = getNextArgument();
	const char* argZ = getNextArgument();

	double X =  (argX) ? std::stof(argX) : 0.0;
	double Y =  (argY) ? std::stof(argY) : 0.0;
	double Z =  (argZ) ? std::stof(argZ) : 0.0;

	translate(X, Y, Z);

	return "Matrix C Translated x=" + std::to_string(X) + ", y=" + std::to_string(Y) + ", z=" + std::to_string(Z);
}

void TranslateModule::translate(const double x, const double y, const double z)
{
	
	Mat4 transMat (	1., 0., 0., x,
					0., 1., 0., y,
					0., 0., 1., z,
					0., 0., 0., 1.);

	*matPtr = *matPtr * transMat;
	
}