#include "rotateModule.h"
#include "mat4.hpp"
#include <cmath>


RotateModule::RotateModule(Mat4* ptr): TransformModule(ptr)
{

}

RotateModule::~RotateModule()
{
	//Nothing
}

std::string RotateModule::invoke(char* line)
{
	const char* argA = getNextArgument();
	const char* argX = getNextArgument();
	const char* argY = getNextArgument();
	const char* argZ = getNextArgument();

	double A = 	(argA) ? std::stof(argA) : 0.0;
	double X =  (argX) ? std::stof(argX) : 0.0;
	double Y =  (argY) ? std::stof(argY) : 0.0;
	double Z =  (argZ) ? std::stof(argZ) : 0.0;

	rotate(A, X, Y, Z);

	return "Matrix C Rotated angle= " + std::to_string(A) 
			+ ", x=" + std::to_string(X) + ", y=" 
			+ std::to_string(Y) + ", z=" + std::to_string(Z);
}

void RotateModule::rotate(const double degrees, const double x, const double y, const double z)
{
	double theta = toRadians(degrees);
	double L = sqrt(x * x + y * y + z * z);
	double x1 = x/L;
	double y1 = y/L;
	double z1 = z/L;
	double x2 = x1 * x1;
	double y2 = y1 * y1;
	double z2 = z1 * z1;
	double c = cos(theta);
	double s = sin(theta);


	double a11 =	x2 * (1-c) + c;
	double a12 = 	x1 * y1 * (1-c) - (z1 * s);
	double a13 =	x1 * z1 * (1-c) + (y1 * s);
	double a14 =	0.0;

	double a21 =	y1 * x1 * (1-c) + (z1 * s);
	double a22 =	y2 * (1-c) + c;
	double a23 = 	y1 * z1 * (1-c) - (x1 * s);
	double a24 =	0.0;

	double a31 =	x1 * z1 * (1-c) - (y1 * s);
	double a32 =	y1 * z1 * (1-c) + (x1 * s);
	double a33 = 	z2 * (1-c) + c;
	double a34 = 	0.0;

	double a41 = 	0.0;
	double a42 = 	0.0;
	double a43 = 	0.0;
	double a44 = 	1.0;

	Mat4 rotMatrix(	a11,a12,a13,a14,
					a21,a22,a23,a24,
					a31,a32,a33,a34,
					a41,a42,a43,a44);

	*matPtr = *matPtr * rotMatrix;	
}