#include "orthoModule.h"
#include "mat4.hpp"
#include <cmath>
#include "src/3D.c"


OrthoModule::OrthoModule(Mat4* ptr, bool* persp): TransformModule(ptr), boolPtr(persp)
{
	ortho(0.0, 1.0, 0.0, 1.0, 0.0001, 10000.0);
}

OrthoModule::~OrthoModule()
{
	//Nothing
}

std::string OrthoModule::invoke(char* line)
{

	const char* argL = getNextArgument();
	const char* argR = getNextArgument();
	const char* argB = getNextArgument();
	const char* argT = getNextArgument();
	const char* argN = getNextArgument();
	const char* argF = getNextArgument();

	double L =  (argL) ? std::stof(argL) : 0.0;
	double R =  (argR) ? std::stof(argR) : 1.0;
	double B =  (argB) ? std::stof(argB) : 0.0;
	double T =  (argT) ? std::stof(argT) : 1.0;
	double N =  (argN) ? std::stof(argN) : 0.0001;
	double F =  (argF) ? std::stof(argF) : 10000.0;

	ortho(L,R,B,T,N,F);
	*boolPtr = false;

	return "Orthographic Matrix Set";
}

void OrthoModule::ortho(const double l, const double r, const double b, const double t, const double n, const double f)
{
	Near = n;
	Far = f;
	double a11 = 2 / (r-l);
	double a14 = -((r+l)/(r-l));

	double a22 = 2 / (t-b);
	double a24 = -((t+b)/(t-b));

	double a33 = -2 / (f - n);
	double a34 = -((f + n) / (f - n));

	Mat4 orthoMat(	a11, 0, 0, a14,
					0, a22, 0, a24,
					0, 0, a33, a34,
					0, 0, 0, 1);

	*matPtr = orthoMat;

}




